package com.TEAF.TestFiles.PageObjects;

import org.apache.log4j.Logger;

import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.stepDefinitions.GalenStep;

public class WindowsCalc {

	static GetPageObjectRead PO = new GetPageObjectRead();
	static Logger log = Logger.getLogger(WindowsCalc.class.getName());

	public static void WinCalcButtons() {
		PO.add("url::http:/Windows/System32/calc.exe");
		PO.add("NumberOne::name=One");
		PO.add("NumberTwo::name=Two");
		PO.add("NumberThree::name=Three");
		PO.add("NumberFour::name=Four");
		PO.add("NumberFive::name=Five");
		PO.add("NumberSix::name=Six");
		PO.add("NumberSeven::name=Seven");
		PO.add("NumberEight::name=Eight");
		PO.add("NumberNine::name=Nine");
		PO.add("NumberZero::name=Zero");
		PO.add("PlusOperator::name=Plus");
		PO.add("MinusOperator::name=Minus");
		PO.add("MultiplyOperator::name=Multiply by");
		PO.add("DivisionOperator::name=Divide by");
		PO.add("EqualTo::name=Equals");
		PO.add("MinusOperator::name=Minus");
		PO.add("CalcResultField::id=CalculatorResults");
	}

	public static void loadallPageObjects() {
		WinCalcButtons();
		log.info("Locator Values Load Complete!");
	}
}
