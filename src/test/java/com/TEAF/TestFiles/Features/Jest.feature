@TEST123
Feature: ES

@Ignore
Scenario: Verify data collection from Customer CrossRef index and store in an array
    Given ES Search 'https://vpc-ticketdetail-mfrm-2nsf3ogk2vjyylzbauxlqvrgta.us-west-2.es.amazonaws.com/'
    And Select and Verify the index 'customer_cross_ref' exists
    Then Get Result as String by passing Query '{"query":{"bool":{"must":[{"match":{"ultimateMasterCustomerCode.keyword":""}},{"range":{"ES_MODIFIEDDATETIME":{"gte":"2020/03/28 22:20:41","lte":"2020/03/28 22:20:41"}}}],"must_not":[ ],"should":[ ]}},"from":0,"size":1000,"sort":[ ],"aggs":{ }}'
    And Print ES Query Response
    When Retrieve Record '$.hits.hits[*]._source.customerCode' from Query Result and store in variable 'TLMMasterCodes'