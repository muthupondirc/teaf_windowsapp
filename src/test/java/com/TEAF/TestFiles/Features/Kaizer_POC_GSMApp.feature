@GSM
Feature: Create Inventory and Suppliers in GSM Application
#Author: <test>@royalcyber.com
 
 Background:
   Given My WindowsApp 'GSM' is open

  @tag1
  Scenario Outline: Verify user is able to add inventory in GSM application
  	
    Given I click 'tab_Inventory' button
    And I click 'New' button
    And I enter '<Comment>' in field 'input_Comment' of window
    And I click 'input_Supplier' button
    And I select value from combobox '<Supplier>'
    And I click 'input_Category' button
    And I select value from combobox '<Category>'
    And I enter '<Reference>' in field 'input_Reference' of window
    And I enter '<Description>' in field 'input_Description' of window
    And I enter '<Quantity>' in field 'input_Quantity' of window
   	And I click 'input_Location' button
    And I select value from combobox '<Location>'
    And I enter '<Alarm>' in field 'input_Alarm' of window
    And I enter '<UnitsBuy>' in field 'input_UnitBuy' of window
    And I enter '<UnitsSell>' in field 'input_UnitSell' of window
 		And I click 'btn_Save' button
 		And I close the application
 		
 		
 		 
 Examples:   
|Comment|Supplier|Category|Reference|Description|Quantity|Location|Alarm|UnitsBuy|UnitsSell|
|Invoice generated for 100 Units|combo_SupplierRC|combo_CategoryMaintenance|RC8109|Electric Fining|100|combo_A4C002|8109|100|10|
|Invoice generated for 50 Units|combo_SupplierRC|combo_CategoryMaintenance|RC8110|Air Compressors|100|combo_A4C002|7810|50|10|

@tag1
Scenario Outline: Verify user is able to add/edit the  Supplier in GSM application
Given I click 'tab_Suppliers' button
And I click 'New' button   
And I enter '<Company>' in field 'input_Company' of window
And I enter '<Barcode>' in field 'input_Barcode' of window
And I enter '<Web>' in field 'input_Web' of window
And I enter '<Address>' in field 'input_Address' of window
And I enter '<ZipCode>' in field 'input_ZipCode' of window
And I enter '<City>' in field 'input_City' of window
And I enter '<Country>' in field 'input_Country' of window
And I enter '<LastName>' in field 'input_LastName' of window
And I enter '<FirstName>' in field 'input_FirstName' of window
And I enter '<Title>' in field 'input_Title' of window
And I enter '<Tel1>' in field 'input_Tel1' of window
And I enter '<Tel2>' in field 'input_Tel2' of window
And I enter '<Fax>' in field 'input_Fax' of window
And I enter '<Email>' in field 'input_Email' of window
And I click 'btn_Save' button
And I select the top row in Suppliers
And I click 'btn_Edit' button
And I enter '<Tel2_updated>' in field 'input_Tel2' of window
And I enter '<Address_updated>' in field 'input_Address' of window
And I click 'btn_Save' button
And I should see text '<Tel2_updated>' present on window at 'cellTel2Row0'
And I should see text '<Address_updated>' present on window at 'cellAddressRow0'
And I close the application

Examples:
|Company|Barcode|Web|Address|ZipCode|City|Country|LastName|FirstName|Title|Tel1|Tel2|Fax|Email|Address_updated|Tel2_updated|
|GeneralElectrics|101010|http://www.ge.com|St John Street|60008|Chicago|US|John|Fernandos|Supplier|022-227733||1010002|supplier.ge.com|St Peter Street|021-13456|
|Tesla|343407|http://www.teska.com|Denmark street|60017|Texas|US|David|Warner|Supplier|011-267345||3434079|supplier.tesla.com|St Santa Street|011-27899|

@tag1
Scenario: Verify user is able to traverse all the tabs
Given I click 'tab_Inventory' button
And I should see element 'btn_Id' present on Window
And I should see element 'btn_Location' present on Window
And I should see element 'btn_Reference' present on Window
And I click 'tab_Suppliers' button
And I should see element 'btn_Company' present on Window
And I should see element 'btn_Barcode' present on Window
And I should see element 'btn_Address' present on Window
And I click 'tab_Logs' button
And I should see element 'btn_Utilisateur' present on Window
And I should see element 'btn_Search' present on Window
And I click 'tab_Labels' button
And I should see element 'txt_Printer' present on Window
And I should see element 'btn_Selectionne' present on Window
And I close the application

@tag11
Scenario Outline: Verify updated tel1 is displayed in the table under Suppliers tab
Given I click 'tab_Suppliers' button
And I should see tel number format is correct for text '<Tel2_updated>' on window at 'cellTel2Row0'
And I should see text '<Tel2_updated>' present on window at 'cellTel2Row0'

Examples:
|Tel2_updated|
|510-5550107|
|510-555-0107|




