package com.TEAF.TestFiles.Runner;

import org.apache.log4j.Logger;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import com.TEAF.framework.Execution;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@CucumberOptions(dryRun = false, plugin = { "pretty",
		"json:src/test/java/com/TestResults/cucumber-report/cucumber_1.json" }, strict = true, junit = "--step-notifications", features = {
				"src/test/java/com/TEAF/TestFiles/Features" }, glue = { "com.TEAF.Hooks",
						"com.TEAF.stepDefinitions" }, tags = { "@tag11", "not @Ignore" }, monochrome = true)

@RunWith(Cucumber.class)
public class TestRunner_1 {

	public static String Platform = "desktop";
	public static String Browser = "REST Service";
	public static String TestName = "RC TEAF";

	static String appType = "windowsapp";
	static String winAppLocation = "C:\\Users\\MuthupondiS\\Documents\\SIM\\SIM\\GSM.exe";
	static String URL = null;

	static Logger log = Logger.getLogger(TestRunner_1.class.getName());

	@BeforeClass
	public static void setUp() {
		String jsonFileName = "cucumber_1";

		Execution.setup(Platform, Browser, URL, appType, winAppLocation, TestName, jsonFileName);

	}

	@AfterClass
	public static void tearDown() throws Throwable {
		String successFulRecipients = "karthik.gobi@royalcyber.com,fakhruddin@royalcyber.com";
		String successFulCCRecipients = "muthupondi.s@royalcyber.com,swathin@royalcyber.com";
		String failureRecipients = "karthik.gobi@royalcyber.com,fakhruddin@royalcyber.com";
		String failureCCRecipients = "muthupondi.s@royalcyber.com,swathin@royalcyber.com";
		//,karthik.gobi@royalcyber.com,fakhruddin@royalcyber.com
		Execution.tearDown(successFulRecipients, successFulCCRecipients, failureRecipients, failureCCRecipients);
	}

}
