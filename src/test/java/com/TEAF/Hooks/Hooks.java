package com.TEAF.Hooks;

import java.io.IOException;
import java.util.Collection;
import java.util.LinkedHashMap;

import org.apache.log4j.Logger;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.AfterStep;
import cucumber.api.java.Before;

/**
 * @ScriptName : Hooks
 * @Description : This class contains
 * @Author : Swathin Ratheendren
 * @Creation Date : September 2016 @Modified Date:
 */
public class Hooks {
	private static WebDriver driver = StepBase.getDriver();
	public static int code;

	public static LinkedHashMap<String, String> scenarioStatus = new LinkedHashMap<String, String>();
	static Logger log = Logger.getLogger(Hooks.class.getName());
	public static Scenario scenario;
	static int scenarioOutline;


	@Before
	public void BeforeScenarioSteps(Scenario scenario) {
		String name = scenario.getName();
		Collection<String> sourceTagNames = scenario.getSourceTagNames();
		System.out.println(" scname "+ name);
		System.out.println(" sourcetag "+ sourceTagNames);
		
		this.scenario = scenario;
		try {
			StepBase.setScenario(scenario);
			if (!System.getProperty("test.browserName").equalsIgnoreCase("REST Service")) {

				if (System.getProperty("test.postScenarioTearDown").equals("true")) {
					StepBase.setUp(System.getProperty("test.platformName"), System.getProperty("test.browserName"));
				}

				try {
					if (!StepBase.testPlatform.equalsIgnoreCase("mobile")) {
						if (System.getProperty("test.disableToastMessage").equals("false")) {

							Utilities.testStatusToastMessage(scenario.getName());

						}
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					log.error(e);
				}
			} else {

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	@AfterStep
	public void afterStep(Scenario s) throws InterruptedException, IOException {
		if (scenario.isFailed()) {

//		    Cookie cookie = new Cookie("zaleniumTestFailed", scenario.getStatus().toString());
//		    driver.manage().addCookie(cookie);
//		    
//		    Cookie cookiesstatus = new Cookie("zaleniumMessage", scenario.getStatus().toString());
//		    driver.manage().addCookie(cookiesstatus);
			if (!System.getProperty("test.browserName").equalsIgnoreCase("REST Service")) {
				if (!StepBase.testPlatform.equalsIgnoreCase("mobile")) {
					if (System.getProperty("test.disableToastMessage").equals("false")) {

						Utilities.testStatusFailToastMessage(scenario.getName());
					}
				}
			}
				if (!System.getProperty("test.appType").equalsIgnoreCase("windowsapp")) {
					final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
					scenario.embed(screenshot, "image/png"); // Stick it to HTML report
					Utilities.takeScreenshot(driver);

//					String currentUrl = StepBase.getDriver().getCurrentUrl();
//					try {
//						URL url = new URL(currentUrl);
//						HttpURLConnection connection = (HttpURLConnection) url.openConnection();
//						connection.setRequestMethod("GET");
//						connection.connect();
//
//						code = connection.getResponseCode();
//						log.info("HTTP Code for failure url : " + code);
//					} catch (ConnectException e) {
//						log.error("Connection Exception code : " + 521);
//						code = 521;
//					} catch (UnknownHostException e) {
//						log.error("UnKnownHost Exception code : " + 520);
//						code = 520;
//
//					} catch (Exception e) {
//						log.error("Http URL Connection Exception");
//					}
				}else {
					scenario.embed(Utilities.getScreenshot(),"image/png");
				}
			}
		}
	

	@After
	public void AfterScenarioSteps(Scenario scenario) {
		try {

			String name = scenario.getName();
			log.debug(name);
			String status = scenario.getStatus().toString();
			log.info("Scenario - " + name + " : status - " + status);

			if (scenarioStatus.containsKey(name)) {
				name = name + " Examples " + scenarioOutline++;
			}
			scenarioStatus.put(name, status);

			if (!System.getProperty("test.browserName").equalsIgnoreCase("REST Service")) {

				if (System.getProperty("test.postScenarioTearDown").equals("true")) {
					StepBase.tearDown();
				}
//				
//			    Cookie cookiesstatus= new Cookie("zaleniumTestPassed", scenario.getStatus().toString());
//			    driver.manage().addCookie(cookiesstatus);
//			    Cookie cookie = new Cookie("zaleniumMessage", scenario.getStatus().toString());
//			    driver.manage().addCookie(cookie);
//			}
				if (scenario.getStatus().toString().equalsIgnoreCase("PASSED")
						&& !StepBase.testPlatform.equalsIgnoreCase("mobile")) {
					if (System.getProperty("test.disableToastMessage").equals("false")) {
						Utilities.testStatusToastPass(scenario.getName());

					}
				}
			}
		} catch (Exception e) {
			log.error(e);

		}
	}
}
