package com.TEAF.stepDefinitions;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

import com.TEAF.framework.StepBase;
import com.galenframework.api.Galen;
import com.galenframework.reports.GalenTestInfo;
import com.galenframework.reports.HtmlReportBuilder;
import com.galenframework.reports.model.LayoutReport;

import cucumber.api.java.en.When;

public class GalenStep{

	static WebDriver driver = StepBase.getDriver();
	static List<LayoutReport> AllLayouts = new ArrayList<LayoutReport>();
	static List<String> allTags = new ArrayList<String>();
	static int width;
	static int height;
	static Logger log = Logger.getLogger(GalenStep.class.getName());

	@When("^I verify Specifcation for '(.*)' for '(.*)' View using Galen$")
	public void homePageLayoutTest(String gspec, String tags) throws IOException {

		System.out.println("Initiating Galen Tests!");
		String path = System.getProperty("user.dir") + "/src/test/java/com/galen/" + gspec + ".gspec";
		LayoutReport layoutReport = Galen.checkLayout(driver, path, Arrays.asList(tags));
		AllLayouts.add(layoutReport);
		allTags.add(tags);

		// htmlReportBuilder.build(tests, "UITestReports");
		if (layoutReport.errors() > 0) {
			log.error("Layout test failed");
			Assert.fail("Layout test failed");
		}
		log.info("Galen Tests Complete!");
	}

	@When("^I rename default report and store as '(.*)'$")
	public void filerename(String tag) {
		String tags = tag.toLowerCase();
		File f = new File(System.getProperty("user.dir") + "\\target\\1-" + tags + ".html");
		String rename = "GalenReports" + tag;
		File newfile = new File(System.getProperty("user.dir") + "\\target\\" + rename + ".html");
		f.renameTo(newfile);
		f.delete();

	}

	@When("^I remove the header to take full page screenshot$")
	public static void dimensio_jn() {
		((JavascriptExecutor) driver).executeScript("var elems = window.document.getElementsByTagName('*');\r\n"
				+ "for(i = 0; i < elems.length; i++) { \r\n" + "        if (window.getComputedStyle) {\r\n"
				+ "             var elemStyle = window.getComputedStyle(elems[i], null); \r\n"
				+ "             if(elemStyle.getPropertyValue('position') == 'fixed' && elems[i].innerHTML.length != 0 ){  \r\n"
				+ "                 elems[i].parentNode.removeChild(elems[i]);\r\n" + "             }\r\n"
				+ "        }}");

		/*
		 * ((JavascriptExecutor)
		 * driver).executeScript("var $header = $(\"#header\");\r\n" +
		 * "var HeaderOffset = $header.position().top;\r\n" +
		 * "$(\"#headerContainer\").css({ height: $header.height() });\r\n" + "\r\n" +
		 * "$(\"#container\").scroll(function() {\r\n" +
		 * "    if($(this).scrollTop() > HeaderOffset) {\r\n" +
		 * "        $header.addClass(\"fixedTop\");\r\n" + "    } else {\r\n" +
		 * "        $header.removeClass(\"fixedTop\");\r\n" + "    }\r\n" + "});");
		 */
	}

	@When("^I resize the screen dimension to '(.*)' width and '(.*)' height$")
	public static void dimension(int Width, int Height) {
		driver.manage().window().setSize(new Dimension(Width, Height));
	}

	@When("^I generate UI Test Reports$")
	public static void generateUIReport() throws IOException {
		List<GalenTestInfo> tests = new LinkedList<GalenTestInfo>();
		
		for (int j = 0; j < allTags.size(); j++) {
			GalenTestInfo test = GalenTestInfo.fromString(allTags.get(j));
			test.getReport().layout(AllLayouts.get(j), allTags.get(j));
			tests.add(test);
		}

		log.info("UI Reports Generated!");
		HtmlReportBuilder htmlReportBuilder = new HtmlReportBuilder();
		htmlReportBuilder.build(tests, "UITestReports");
	}
}
