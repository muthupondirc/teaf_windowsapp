package com.TEAF.stepDefinitions;

import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import javax.imageio.ImageIO;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;
import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.LocatorGenerator;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;
import com.gargoylesoftware.htmlunit.ElementNotFoundException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.CucumberException;
import cucumber.runtime.StepDefinitionMatch;

/**
 * @ScriptName : Utilities
 * @Description : This class contains Commonly used Keyword for Mobile/Web
 *              application automation using Cucumber framework
 * @Author : Swathin Ratheendren
 * @Creation Date : September 2016 @Modified Date:
 */
public class CommonSteps {

	static Logger log = Logger.getLogger(CommonSteps.class.getName());

	private static WebDriver driver;
	private static BufferedImage bufferedImage;
	private static byte[] imageInByte = null;
	private static int elementWaitTime = Integer.parseInt(System.getProperty("test.implicitlyWait"));
	public static String appName = null;


	@Given("^My WebApp '(.*)' is open$")
	public static void my_webapp_is_open(String url) throws Exception {
		try {
			appName = url;
			driver = StepBase.getDriver();
			log.info("Driver value: " + driver);
			driver.get(GetPageObjectRead.OR_GetURL(url));
			WrapperFunctions.waitForPageToLoad();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Given("^I get all element locators under '(.*)' : '(.*)' on section : '(.*)' to file name: '(.*)'$")
	public static void i_capture_locators_in_the_page(String key, String value, String section, String fileName)
			throws Throwable {
		try {
			log.info(driver.getCurrentUrl());
			LocatorGenerator.createExcel(appName, section, fileName, driver.getCurrentUrl());
			LocatorGenerator.getTagChildElementsbyAttributeKeyValue(driver.getPageSource(), key, value, true);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			throw new CucumberException(e);
		}
	}
	
	@When("^I navigate to '(.*)' application$")
	public static void i_navigate_to_application(String url) {
		try {
//			String UpdateUrl = url;
			driver.navigate().to(url);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new CucumberException(e.getMessage(), e);
		}
	}
	
	

	@Given("^My NativeApp '(.*)' is open$")
	public static void my_nativeapp_is_open(String app) throws Exception {
		try {
			driver = StepBase.getDriver();
			if (System.getProperty("test.pageObjectMode").equalsIgnoreCase("csv")) {
				GetPageObjectRead.ReadCSV(app);

			} else if (System.getProperty("test.pageObjectMode").equalsIgnoreCase("class")) {
				GetPageObjectRead.ReadPOClass(app);
			} else if (System.getProperty("test.pageObjectMode").equalsIgnoreCase("xlsx")) {
				GetPageObjectRead.ReadExcel(app);
			}
			Thread.sleep(15000);
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I navigate to '(.*)' page$")
	public static void I_NavigateTo(String url) {
		try {
			driver.navigate().to(HashMapContainer.getPO(url));
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I refresh the WebPage$")
	public static void I_Refresh_WebPage() {
		try {
			driver.navigate().refresh();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I focus and click '(.*)'$")
	public static void I_focus_click(String element) {
		try {
			Actions ac = new Actions(driver);
			// WebElement findElement =
			// driver.findElement(GetPageObjectRead.OR_GetElement(element));
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			ac.moveToElement(wElement).click().build().perform();

		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I wait for visibility of element '(.*)'$")
	public static void I_wait_for_visibility_of_element(String element) {

		WrapperFunctions.waitForElementVisibility(GetPageObjectRead.OR_GetElement(element));
	}

	@Then("^I enter '(.*)' in field '(.*)'$")
	public static void I_enter_in_field(String value, String element) {
		try {
			if (value.length() > 3) {
				if (value.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					value = HashMapContainer.get(value);
				} else if (value.substring(0, 3).equals("$XS")) {
					String randomVal = value.replaceAll("\\" + value.substring(0, 4),
							Utilities.getRandomString(Character.getNumericValue(value.charAt(3))));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				} else if (value.substring(0, 3).equals("$XE")) {
					String randomVal = Utilities.GetRandomEmail(Character.getNumericValue(value.charAt(3)));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				} else if (value.substring(0, 3).equals("$XN")) {
					String randomVal = value.replaceAll("\\" + value.substring(0, 4),
							Utilities.getRandomNum(Character.getNumericValue(value.charAt(3))));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				} else if (value.substring(0, 3).equals("$XA")) {
					String randomVal = value.replaceAll("\\" + value.substring(0, 4),
							Utilities.getRandomAlphaNumeric(Character.getNumericValue(value.charAt(3))));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.sendKeys(value);
			// TODO Dismiss keyboard for webView
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I enter '(.*)' in the feild '(.*)' using actions$")
	public static void i_enter_in_the_feild_using_actions(String value, String element) {

		try {
			WebElement inputElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (inputElement.isDisplayed() && inputElement.isEnabled()) {
				Actions ac = new Actions(driver);
				ac.sendKeys(inputElement, value).build().perform();

			} else {
				throw new RuntimeException();

			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			throw new RuntimeException();

		}
	}

	@Then("^I clear the text and enter '(.*)' in field '(.*)' by JS$")
	public static void i_clear_and_enter_the_text_js(String value, String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("arguments[0].setAttribute('value', '" + value + "')", wElement);

		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@When("^I should see the selected value '(.*)' in the drop down '(.*)'$")
	public static void i_should_see_selected_value_in_the_dropdown(String value, String element) throws Exception {
		try {
			if (value.startsWith("$$")) {
				value = HashMapContainer.get(value);
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);

			Select sc = new Select(wElement);
			String text = sc.getFirstSelectedOption().getText();
			Assert.assertEquals(value, text);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			throw new RuntimeException();
		}

	}

	@Then("^I clear the text and enter '(.*)' in field '(.*)'$")
	public static void i_clear_and_enter_the_text(String value, String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);

			wElement.clear();
			if (value.length() > 3) {
				if (value.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					value = HashMapContainer.get(value);
				} else if (value.substring(0, 3).equals("$XS")) {
					String randomVal = value.replaceAll("\\" + value.substring(0, 4),
							Utilities.getRandomString(Character.getNumericValue(value.charAt(3))));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				} else if (value.substring(0, 3).equals("$XE")) {
					String randomVal = Utilities.GetRandomEmail(Character.getNumericValue(value.charAt(3)));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				} else if (value.substring(0, 3).equals("$XN")) {
					String randomVal = value.replaceAll("\\" + value.substring(0, 4),
							Utilities.getRandomNum(Character.getNumericValue(value.charAt(3))));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				} else if (value.substring(0, 3).equals("$XA")) {
					String randomVal = value.replaceAll("\\" + value.substring(0, 4),
							Utilities.getRandomAlphaNumeric(Character.getNumericValue(value.charAt(3))));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				}
			}
			wElement.sendKeys(value);
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I clear field '(.*)'$")
	public static void I_clear_Field(String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.clear();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I hit enter-key on element '(.*)'$")
	public static void I_hit_key_on_element(String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.sendKeys(Keys.ENTER);
			// TODO Dismiss keyboard for webView
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I hit down Arrow key on element '(.*)'$")
	public static void I_hit_Downkey_on_element(String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.sendKeys(Keys.ARROW_DOWN);
			// TODO Dismiss keyboard for webView
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I mouse over '(.*)'$")
	public static void I_mouse_over(String element) {
		try {
			Actions action = new Actions(driver);
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			action.moveToElement(wElement).build().perform();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I verify tool tip message '(.*)' for element '(.*)'$")
	public static void I_Verify_ToolTip_Message(String expectedMessage, String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			/*
			 * Actions action = new Actions(driver); WebElement we =
			 * driver.findElement(GetPageObject.OR_GetElement(element));
			 * action.moveToElement(we).build().perform();
			 */
			String ToolTipMessage = wElement.getAttribute("title");
			Assert.assertEquals(expectedMessage, ToolTipMessage);
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I verify value '(.*)' in field '(.*)'$")
	public static void I_Verify_value_inField(String expectedValue, String element) {
		try {
			if (expectedValue.length() > 1) {
				if (expectedValue.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					expectedValue = HashMapContainer.get(expectedValue);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);

			String ActualValue = wElement.getAttribute("value");
			Assert.assertEquals(expectedValue, ActualValue,
					"Actual value does not match the expected value in specified field!");
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I verify element '(.*)' is disabled$")
	public static void I_Verify_Element_isDisabled(String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (wElement.isEnabled()) {
				throw new Exception("Element is enabled!");
			}
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I enter '(.*)' into RichText Editor '(.*)'$")
	public static void I_enter_into_RichTextEditor(String value, String element) {
		try {
			driver.switchTo().frame("tinymce");
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.clear();
			wElement.sendKeys(value);
			driver.switchTo().defaultContent();
			// TODO Dismiss keyboard for webView
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Given("^I click '(.*)'$")
	public static void I_click(String element) {
		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.click();
			StepBase.embedScreenshot();
			// Utilities.takeScreenshot(driver);
			WrapperFunctions.waitForPageToLoad();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Given("^I double-click '(.*)'$")
	public static void I_doubleClick(String element) {
		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			Actions act = new Actions(driver);
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			act.doubleClick(wElement).build().perform();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Given("^I take screenshot$")
	public static void take_Screenshot() {
		try {
			StepBase.embedScreenshot();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Given("^I click link '(.*)'$")
	public static void I_click_link(String linkText) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(linkText, By.linkText(linkText),
					elementWaitTime);
			wElement.click();
			WrapperFunctions.waitForPageToLoad();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I drag field '(.*)' and drop '(.*)'$")
	public static void I_drag_Field(String element, String element2) {
		try {
			WebElement source = WrapperFunctions.getElementByLocator(element, GetPageObjectRead.OR_GetElement(element),
					elementWaitTime);
			WebElement target = WrapperFunctions.getElementByLocator(element2,
					GetPageObjectRead.OR_GetElement(element2), elementWaitTime);
			Actions ac = new Actions(driver);
			ac.dragAndDrop(source, target).build().perform();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Given("^I click alert accept$")
	public static void I_click_alert_accept() {
		try {
			Alert alert = driver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I click by JS '(.*)'$")
	public static void I_clickJS(String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.isEnabled();
			wElement.isDisplayed();
			WrapperFunctions.clickByJS(wElement);
			StepBase.embedScreenshot();
			WrapperFunctions.waitForPageToLoad();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I should see element '(.*)' present on page$")
	public static void I_should_see_on_page(String element) {

		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (wElement.isDisplayed()) {
				WrapperFunctions.highLightElement(wElement);
			} else {
				throw new Exception("Element is not found! :" + element);
			}

		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}
	
	@Then("^I should see element '(.*)' is disabled on page$")
	public static void I_should_see_disbaled_on_page(String element) {

		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (!wElement.isEnabled()) {
				WrapperFunctions.highLightElement(wElement);
			} else {
				throw new Exception("Element is not disabled! :" + element);
			}

		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I should see element '(.*)' present on page_$")
	public static void I_should_see_on_page_AndScreenshot(String element) {

		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (wElement.isDisplayed()) {
				WrapperFunctions.highLightElement(driver.findElement(GetPageObjectRead.OR_GetElement(element)));
				StepBase.embedScreenshot();
				// Utilities.takeScreenshot(driver);
			} else {
				throw new Exception("Element is not found!");
			}

		} catch (Exception e) {
			
			log.info("Element is not found");
			throw new ElementNotFoundException(element, "", "");
		}
	}

	@Then("^I should not see element '(.*)' present on page$")
	public static void I_should_not_see_on_page(String element) throws Exception {

		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			log.info("Element is displayed: " + wElement.isDisplayed());
			if (wElement.isDisplayed()) {
				StepBase.embedScreenshot();
				WrapperFunctions.highLightElement(driver.findElement(GetPageObjectRead.OR_GetElement(element)));
				throw new Exception("Element is found on page!");
			}

		} catch (Exception e) {
			
			throw new Exception("Element is found on Page!");
		}
	}

	@Then("^I should see text '(.*)' present on page$")
	public static void I_should_see_text_present_on_page(String expectedText) {
		try {
			if (expectedText.length() > 1) {
				if (expectedText.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					expectedText = HashMapContainer.get(expectedText);
				}
			}
			if (driver.getPageSource().contains(expectedText)) {
				log.info("Text " + expectedText + " found on page!");
				StepBase.embedScreenshot();
			} else {
				throw new ElementNotFoundException(expectedText, " ", " ");
			}
		} catch (Exception e) {
			
			throw new ElementNotFoundException(expectedText, " ", " ");
		}
	}

	@Then("^I verify checkbox '(.*)' is '(.*)'$")
	public static void I_verify_checkBox_is_Checked(String element, String status) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			if (status.equalsIgnoreCase("checked")) {
				if (!wElement.isSelected()) {
					throw new Exception("Specified Checkbox is not checked!");
				}
			} else if (status.equalsIgnoreCase("unchecked")) {
				if (wElement.isSelected()) {
					throw new Exception("Specified Checkbox is checked!");
				}
			}
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I switch to iFrame '(.*)'$")
	public static void I_switchTo_iFrame(String FrameID) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(FrameID,
					GetPageObjectRead.OR_GetElement(FrameID), elementWaitTime);
			driver.switchTo().frame(wElement);
			StepBase.embedScreenshot();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}

	}

	@Then("^I switch to default content$")
	public static void I_switchTo_DefaultContent() {
		try {
			driver.switchTo().defaultContent();
			StepBase.embedScreenshot();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}

	}

	@Then("^I switch back to Main Window$")
	public static void I_switchTo_MainWindow() {
		try {
			driver.switchTo().window(driver.getWindowHandle());
			StepBase.embedScreenshot();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}

	}

	@Then("^I should see text '(.*)' present on page at '(.*)'$")
	public static void I_should_see_text_present_on_page_At(String expectedText, String element) {
		try {
			if (expectedText.length() > 1) {
				if (expectedText.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					expectedText = HashMapContainer.get(expectedText);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualText = wElement.getText();
			WrapperFunctions.highLightElement(wElement);
			Assert.assertEquals(expectedText, actualText);
			StepBase.embedScreenshot();
		} catch (Exception e) {
			
			throw new CucumberException(e);
			
		}
	}

	@Then("^I should see text '(.*)' contained on page at '(.*)'$")
	public static void I_should_see_text_contained_on_page_At(String expectedText, String element) {
		try {
			if (expectedText.length() > 1) {
				if (expectedText.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer! " + HashMapContainer.get(expectedText));
					expectedText = HashMapContainer.get(expectedText);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualText = wElement.getText();
			WrapperFunctions.highLightElement(wElement);
			log.info("Actual " + actualText + " Expected: " + expectedText);

			try {
				Assert.assertTrue(expectedText.toLowerCase().contains((actualText.toLowerCase())));
			} catch (AssertionError e) {
				Assert.assertEquals(expectedText, actualText);
			}
			StepBase.embedScreenshot();
		} catch (Exception e) {
			
			throw new CucumberException(e.getMessage(), e);
		}
	}

	@Given("^I should see variable '(.*)' value contained in expected value '(.*)'$")
	public static void i_should_see_element_text_present_on_expected_value(String expecString, String element) {
		try {
			if (expecString.length() > 1) {
				if (expecString.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer! " + HashMapContainer.get(expecString));

					expecString = HashMapContainer.get(expecString);
				}
			}
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualText = wElement.getText();
			WrapperFunctions.highLightElement(wElement);
			log.info("Actual " + actualText + " Expected: " + expecString);

			try {
				Assert.assertTrue(actualText.toLowerCase().contains((expecString.toLowerCase())));
			} catch (AssertionError e) {
				Assert.assertEquals(expecString, actualText);

			}
			StepBase.embedScreenshot();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I should see text matching regx '(.*)' present on page at '(.*)'$")
	public static void I_should_see_text_matching_regularExpression(String regx, String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualText = wElement.getText();
			// Assert.assertTrue(actualText.matches(regx),"Actual Text Found:
			// "+actualText+"|");
			Assert.assertTrue(actualText.matches(regx));
			StepBase.embedScreenshot();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}

	}

	@Then("^I wait '(.*)' seconds for presence of element '(.*)'$")
	public static void I_wait_for_presence_of_element(int seconds, String element) {
		try {
			WrapperFunctions.waitForElementPresence(GetPageObjectRead.OR_GetElement(element), seconds);
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I wait for '(.*)' seconds$")
	public static void I_pause_for_seconds(int seconds) {
		try {
			Thread.sleep(seconds * 1000);
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

		
	@Then("^I switch to window with title '(.*)'$")
	public static void I_switch_to_window_with_title(String title) {
		try {
			if (title.length() > 1) {
				if (title.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					title = HashMapContainer.get(title);
				}
			}
			WrapperFunctions.switchToWindowUsingTitle(title);
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}

	}

	@Then("^I select option '(.*)' in dropdown '(.*)' by '(.*)'$")
	public static void I_select_option_in_dd_by(String option, String element, String optionType) {
		Assert.assertTrue(
				WrapperFunctions.selectDropDownOption(GetPageObjectRead.OR_GetElement(element), option, optionType));
	}

	@Then("^I scroll to '(.*)' - '(.*)'$")
	public static void I_scroll_to_element(String scrolltype, String element) {
		try {
			WrapperFunctions.scroll(scrolltype, element);
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I scroll through page$")
	public static void I_scroll_thru_page() {
		try {
			for (int i = 0; i <= 16; i++) {
				WrapperFunctions.scroll("coordinates", "0,200");
				Thread.sleep(1000);
			}
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I scroll to top of the page$")
	public static void i_scroll_through_top_page() {
		try {
			WrapperFunctions.scroll("top", "");
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I store text '(.*)' as '(.*)'$")
	public static void I_get_text_from(String TextValue, String ValueName) {
		try {
			HashMapContainer.add("$$" + ValueName, TextValue);
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I get text from '(.*)' and store$")
	public static void I_get_text_from(String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String value = wElement.getText();
			HashMapContainer.add("$$" + element, value);

		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I enter from stored variable '(.*)' into feild '(.*)'$")
	public static void I_enter_from_StoredValue(String StoredValue, String element) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			wElement.sendKeys(HashMapContainer.get(StoredValue));
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I compare '(.*)' with stored value '(.*)'$")
	public static void I_compare_with_StoredValue(String element, String storedValue) {
		try {
			WebElement wElement = WrapperFunctions.getElementByLocator(element,
					GetPageObjectRead.OR_GetElement(element), elementWaitTime);
			String actualValue = wElement.getText();
			storedValue = HashMapContainer.get(storedValue);
			Assert.assertEquals(storedValue, actualValue,
					"I Compare " + actualValue + " with expected value " + storedValue);
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

	@Then("^I verify screenshot to Baseline of page '(.*)'$")
	public static void I_verify_Screenshot_toBaselined(String ImageBaseline) throws Exception {
		try {
			File f1 = new File(Utilities.takeScreenshot(driver));
			log.info("ActualSS Path: " + f1);
			File f2 = new File(ImageBaseline);
			Float PercentageVariation = Utilities.compareImage(f1, f2);
			log.info("% of variation in image comparison with Baseline image: " + PercentageVariation + "%");
			if (PercentageVariation != 0.0) {
				bufferedImage = Utilities.getDifferenceImage(f1, f2);
				ByteArrayOutputStream baos = new ByteArrayOutputStream();
				ImageIO.write(bufferedImage, "jpg", baos);
				baos.flush();
				imageInByte = baos.toByteArray();
				baos.close();
				throw new Exception("Compared images do not match!" + imageInByte);
			}
		} catch (Exception e) {
			
			// StepBase.embedProvidedScreenshot(imageInByte);
			throw new CucumberException(e);
		}
	}

	@Then("^I close browser$")
	public static void I_Close_Browser() {
		try {
			driver.close();
		} catch (Exception e) {
			
			throw new CucumberException(e);
		}
	}

}
