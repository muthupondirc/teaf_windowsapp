package com.TEAF.stepDefinitions;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.List;

import org.apache.log4j.Logger;
import org.junit.Assert;

import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.RestAssuredUtility;
import com.TEAF.framework.ElasticSearchJestUtility;
import com.jayway.jsonpath.Configuration;

import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class ElasticSearchJestSteps {
	static Logger log = Logger.getLogger(ElasticSearchJestSteps.class.getName());

	@When("^ES Search '(.*)'$")
	public static void jestSearch(String url) throws Throwable {
		ElasticSearchJestUtility.jestClient(url);
	}

	@When("^Select and Verify the index '(.*)' exists$")
	public static void select_verify_the_index(String index) throws IOException {
		ElasticSearchJestUtility.getIndicesExists(index);
	}

	@When("^Get Result as String by passing Query '(.*)'$")
	public static void get_result_as_string_by_passing_query(String query) throws Throwable {
		ElasticSearchJestUtility.searchQuerywithIndex(query);
		Object read = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), "$.hits.total");
		log.info("No of Records found in DB : " + read);
	}

	@When("^Print ES Query Response$")
	public static void elastic_search_print_response() {
		log.info(ElasticSearchJestUtility.getJestResponseAsString());
	}

	@When("^Query Key '(.*)' present in the response$")
	public void key_count_present_in_the_response(String key) {
		try {
			String read = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), key);
			if (read.length() > 1) {
				log.info("Query Key " + key + "Present in the Response");
			} else {
				log.info("Query Key " + key + " Not Present in the Response");
			}
		} catch (Exception e) {
			log.info("Query Key " + key + " Not Present in the Response");

		}
	}

	@Then("^Verify query result contains Key '(.*)' & Value '(.*)'$")
	public void match_JSONPath_contains(String path, String Expected) {
		if (Expected.startsWith("$$")) {
			Expected = HashMapContainer.get(Expected);
		}

		String Actual = ElasticSearchJestUtility.jud.getJestResponse();
		Object document = Configuration.defaultConfiguration().jsonProvider().parse(Actual);

		try {
			List<Object> actual;

			actual = com.jayway.jsonpath.JsonPath.read(document, path);
			if (actual.size() == 1) {
				String singledata = String.valueOf(actual.get(0));
				assertEquals(" Json Path Value Check ", Expected,
						singledata.toString().replace("[", "").replace("]", "").toString());

			}
			assertEquals(" Json Path Value Check ", Expected,
					actual.toString().replace("[", "").replace("]", "").toString());

		} catch (java.lang.ClassCastException e) {
			String actual;

			actual = String.valueOf(com.jayway.jsonpath.JsonPath.read(document, path));
			assertEquals(" Json Path Value Check ", Expected, actual);

		}

	}

	@When("^Records '(.*)' count present in the DB - '(.*)'$")
	public void records_count_present_in_the_response(String key, String count) {
		List<String> read = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), key);
		Assert.assertEquals(Integer.parseInt(count), read.size());
	}

	@When("^Retrieve Record '(.*)' from Query Result and store in variable '(.*)'$")
	public void retrive_query_data_store(String key, String store) throws Exception {
		Object read = com.jayway.jsonpath.JsonPath.read(ElasticSearchJestUtility.jud.getJestResponse(), key);
		System.out.println(read);
		HashMapContainer.add("$$" + store, read.toString());
	}

}
