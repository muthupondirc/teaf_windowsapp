package com.TEAF.stepDefinitions;

import java.util.Set;

import org.apache.log4j.Logger;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.touch.TouchActions;
import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;
import com.google.common.collect.ImmutableMap;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import cucumber.runtime.CucumberException;
import io.appium.java_client.AppiumDriver;
import io.appium.java_client.MobileElement;
import io.appium.java_client.PerformsTouchActions;
import io.appium.java_client.TouchAction;
import io.appium.java_client.touch.offset.PointOption;

@SuppressWarnings("unchecked")
public class CommonStepsMobile {
	
	static Logger log = Logger.getLogger(CommonStepsMobile.class.getName());


	static StepBase sb = new StepBase();
	static WebDriver driver = StepBase.getDriver();
	static Utilities util = new Utilities();
	static int elementWaitTime = Integer.parseInt(System.getProperty("test.implicitlyWait"));

	@Then("^I touch '(.*)'$")
	public static void Tap_SearchSoftKey(String element) {
		try {
			// TouchActions action = new TouchActions(driver);
			// action.singleTap(WrapperFunctions.getElementByLocator(element,GetPageObjectRead.OR_GetElement(element),
			// elementWaitTime));
			// action.perform();
			WrapperFunctions.getElementByLocator(element, GetPageObjectRead.OR_GetElement(element), elementWaitTime)
					.click();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e);
		}
	}

	@When("^I click on Alert accept$")
	public static void i_click_on_alert_accept() {
		driver.switchTo().alert();
	}
	
	@Then("^I submit form$")
	public static void mobile_SubmitForm() throws Exception {
		try {
			((AppiumDriver<MobileElement>) (driver)).executeScript("mobile:performEditorAction",
					ImmutableMap.of("action", "done"));
		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e);
			throw new CucumberException(e);

		}

	}

	@Then("^I swipe right$")
	public static void Swipe_direction() {
		try {
			TouchActions action = new TouchActions(driver);
			action.flick(10, 15).perform();

		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e);
		}
	}

	@SuppressWarnings("rawtypes")
	@Then("^I swipe from x '(.*)' y '(.*)' to x '(.*)' y '(.*)'$")
	public static void Swipe_direction(int startx, int starty, int endx, int endy) {
		try {

			TouchAction action = new TouchAction((PerformsTouchActions) driver);
			action.longPress(PointOption.point(startx, starty)).moveTo(PointOption.point(endx, endy)).release()
					.perform();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e);
		}
	}

/*	@Then("^I switch context to Native App$")
	public static void Switch_Context_toChromeBrowser() {
		try {

			((AppiumDriver<MobileElement>) (driver)).context("NATIVE_APP");
		//	log.info("NATIVE_APP:" + NATIVE_APP);

		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e);
		}
	}*/
	

	@Then("^I switch context to Native App$")
	public static void Switch_Context_toNativApp() {
		try {
			Set<String> contextNames = ((AppiumDriver<MobileElement>) (driver)).getContextHandles();
			for (String contextName : contextNames) {
				log.info("NativeApp:" + contextName);
				if (contextName.contains("NATIVE")) {
					((AppiumDriver<MobileElement>) (driver)).context(contextName);
					log.info("Native Context:" + contextName);
				}
			}
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e);
		}
	}

}
