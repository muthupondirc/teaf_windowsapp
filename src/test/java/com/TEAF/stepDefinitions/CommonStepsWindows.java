package com.TEAF.stepDefinitions;


import cucumber.runtime.CucumberException;
import io.appium.java_client.windows.WindowsDriver;

import java.awt.AWTException;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Driver;
import java.util.concurrent.ThreadLocalRandom;

import javax.imageio.ImageIO;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.winium.DesktopOptions;
import org.openqa.selenium.winium.WiniumDriver;
//import org.sikuli.script.FindFailed;
//import org.sikuli.script.Pattern;
//import org.sikuli.script.Screen;

import com.TEAF.framework.GetPageObjectRead;
import com.TEAF.framework.HashMapContainer;
import com.TEAF.framework.StepBase;
import com.TEAF.framework.Utilities;
import com.TEAF.framework.WrapperFunctions;

import cucumber.api.Scenario;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;

/**
 * 
 * @ScriptName : Utilities
 * @Description : This class contains Commonly used Keyword for Mobile/Web
 *              application automation using Cucumber framework
 * @Author : Swathin Ratheendren
 * @Creation Date : September 2016 @Modified Date:
 */
public class CommonStepsWindows {

	private static WrapperFunctions wrapFunc = new WrapperFunctions();
	public static WiniumDriver winDriver = null;
	//static WindowsDriver<?> winDriver = null;
//	public static WindowsDriver winDriver;
	static Logger log = Logger.getLogger(CommonStepsWindows.class.getName());

	
	@Given("^My WinApp '(.*)' is open$")
	public static void my_wineapp_is_open(String app) throws Exception {
		try {
			DesiredCapabilities appCapabilities = new DesiredCapabilities();
			appCapabilities.setCapability("app", "C:\\Program Files (x86)\\Microsoft Dynamics\\GP2016\\Dynamics.exe");
			//winDriver = new WindowsDriver(new URL("http://127.0.0.1:4723"), appCapabilities);
			if (System.getProperty("test.pageObjectMode").equalsIgnoreCase("csv")) {
				GetPageObjectRead.ReadCSV(app);

			} else if (System.getProperty("test.pageObjectMode").equalsIgnoreCase("class")) {
				GetPageObjectRead.ReadPOClass(app);
			} else if (System.getProperty("test.pageObjectMode").equalsIgnoreCase("xlsx")) {
				GetPageObjectRead.ReadExcel(app);
			}
			Thread.sleep(30000);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage().substring(0,100), e);
		}
	}

	@Given("^My WindowsApp '(.*)' is open$")
	public static void my_WindowsApp_is_open(String url) throws Exception {
		try {

			DesktopOptions option = new DesktopOptions();

			String AppPath = System.getProperty("test.winDesktopApp");
			option.setApplicationPath(AppPath);

			winDriver = new WiniumDriver(new URL("http://localhost:9999"), option);
			GetPageObjectRead.OR_GetURL(url);
			Thread.sleep(10000);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage().substring(0,100), e);
		}
	}

	@Given("I enter {string} in field {string} of window")
	public static void I_enter_in_field_ofWindow(String value, String element) {
		try {
			// wrapFunc.waitForElementPresence(GetPageObjectRead.OR_GetElement(element));
			// winDriver.findElement(GetPageObject.OR_GetElement(element)).clear();
			if (value.length() > 3) {
				if (value.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					value = HashMapContainer.get(value);
				} else if (value.substring(0, 3).equals("$XS")) {
					String randomVal = value.replaceAll("\\" + value.substring(0, 4),
							Utilities.getRandomString(Character.getNumericValue(value.charAt(3))));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				} else if (value.substring(0, 3).equals("$XE")) {
					String randomVal = Utilities.GetRandomEmail(Character.getNumericValue(value.charAt(3)));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				} else if (value.substring(0, 3).equals("$XN")) {
					String randomVal = value.replaceAll("\\" + value.substring(0, 4),
							Utilities.getRandomNum(Character.getNumericValue(value.charAt(3))));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				} else if (value.substring(0, 3).equals("$XA")) {
					String randomVal = value.replaceAll("\\" + value.substring(0, 4),
							Utilities.getRandomAlphaNumeric(Character.getNumericValue(value.charAt(3))));
					HashMapContainer.add("$$" + value, randomVal);
					value = randomVal;
				}
				else if (value.equals("randomText")) {
					 int rand_int1 = ThreadLocalRandom.current().nextInt(); 
					String randomVal = value.replaceAll(value,
							Integer.toString(rand_int1));
					value = "RC"+randomVal+"_TEST";
				}
			}
			winDriver.findElement(GetPageObjectRead.OR_GetElement(element)).sendKeys(value);
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage().substring(0,100), e);
		}
	}

	@Then("^I verify value '(.*)' in field '(.*)' of window$")
	public static void I_Verify_value_inField_ofWindow(String expectedValue, String element) {
		try {
			if (expectedValue.length() > 1) {
				if (expectedValue.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					expectedValue = HashMapContainer.get(expectedValue);
				}
			}
			//wrapFunc.waitForElementPresence(GetPageObjectRead.OR_GetElement(element));
			String ActualValue = winDriver.findElement(GetPageObjectRead.OR_GetElement(element)).getText();
			StepBase.embedScreenshot();
			Assert.assertEquals(expectedValue, ActualValue,
					"Actual value does not match the expected value in specified field!");
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage().substring(0,100), e);
		}
	}

	@Given("^I click '(.*)' button$")
	public static void I_click_button_ofWindow(String element) {
		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			System.out.println("element"+winDriver.findElement(GetPageObjectRead.OR_GetElement(element)).isDisplayed());
			winDriver.findElement(GetPageObjectRead.OR_GetElement(element)).click();
			//StepBase.embedScreenshot();
			StepBase.winApp_EmbedScreenshot(Utilities.getScreenshot());
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage().substring(0,100), e);
		}
	}

	@Given("I double-click {string} button")
	public static void I_doubleClick_button(String element) {
		try {
			if (element.length() > 1) {
				if (element.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					element = HashMapContainer.get(element);
				}
			}
			// wrapFunc.waitForElementPresence(GetPageObjectRead.OR_GetElement(element));
			Actions act = new Actions(winDriver);
			WebElement E = winDriver.findElement(GetPageObjectRead.OR_GetElement(element));
			act.doubleClick(E).build().perform();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage().substring(0,100), e);
		}
	}

	@Given("^I take screenshot of window$")
	public static void take_Screenshot_ofWindow() {
		try {
			StepBase.embedScreenshot();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage().substring(0,100), e);
		}
	}

	@Given("^I click alert accept on Window$")
	public static void I_click_alert_accept_onWindow() {
		try {
			Alert alert = winDriver.switchTo().alert();
			alert.accept();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage().substring(0,100), e);
		}
	}

	@Given("^I start screen recording$")
	public static void I_start_videoRecording() {
		try {
			Utilities.startVideoRecorder();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage().substring(0,100), e);
		}
	}

	@Given("^I stop screen recording$")
	public static void I_stop_videoRecording() {
		try {
			Utilities.stopVideoRecorder();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage().substring(0,100), e);
		}
	}

	@Then("I should see text {string} present on window at {string}")
	public static void I_should_see_text_present_on_window_At(String expectedText, String element) {
		try {
			if (expectedText.length() > 1) {
				if (expectedText.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					expectedText = HashMapContainer.get(expectedText);
				}

			}
			String actualText = winDriver.findElement(GetPageObjectRead.OR_GetElement(element)).getText();
			// actualText = actualText.substring(11);
			System.out.println(actualText);
			if(expectedText.contains("-")) {
				
			}
				
			Assert.assertEquals(expectedText, actualText);
			// highLightElement(winDriver.findElement(GetPageObjectRead.OR_GetElement(location)));

			StepBase.embedScreenshot();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage().substring(0,100), e);
		}
	}

	@Then("^I should see text contained '(.*)' present on window at '(.*)'$")
	public static void I_should_see_text_contained_present_on_window_At(String expectedText, String location) {
		try {
			if (expectedText.length() > 1) {

				if (expectedText.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					expectedText = HashMapContainer.get(expectedText);
				}
			}
			String actualText = winDriver.findElement(GetPageObjectRead.OR_GetElement(location)).getAttribute("Name");

			// actualText = actualText.substring(11);
			if (actualText.contains(expectedText)) {
				Assert.assertTrue(true);
				// mouseHovertoWindowsLocator(location);
				// highLightElement(winDriver.findElement(GetPageObjectRead.OR_GetElement(location)));

			} else {
				Assert.fail("Expected contained text is " + expectedText + " but Actual is " + actualText);
			}
			StepBase.embedScreenshot();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage().substring(0,100), e);
		}
	}
@Given("I mouseover to an element {string}")
	public static void mouseHovertoWindowsLocator(String element) {
		try {
			//System.out.println(winDriver.findElementByName("Sales").getAttribute("Name"));
			Actions ac = new Actions(winDriver);
			ac.moveToElement(winDriver.findElement(GetPageObjectRead.OR_GetElement(element))).pause(5).build()
					.perform();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.error(e);
			throw new CucumberException(e.getMessage().substring(0,100), e);
		}
	}

	public static void highLightElement(WebElement element) throws Exception {
		try {
			if (System.getProperty("test.highlightElements").equals("true")) {

				winDriver.executeScript(
						"\"automation: ValuePattern.SetValue\", BoundingRectangle, \"{l:0 t:75 r:250 b:706}\"",
						element);

			}
		} catch (Exception e) {
			log.error(e);
		}
	}

	@Then("^I should see element '(.*)' present on Window$")
	public static void I_should_see_on_window(String element) {

		try {
			/*
			 * if (element.length() > 1) { if (element.substring(0, 2).equals("$$")) {
			 * log.info("Fetching from HMcontainer!"); element =
			 * HashMapContainer.get(element); } }
			 */
			log.info("Element!!!! " + element);
			// wrapFunc.waitForElementPresence(GetPageObjectRead.OR_GetElement(element));
			WebElement x = winDriver.findElement(GetPageObjectRead.OR_GetElement(element));
			if (x.isDisplayed()) {

				// highLightElement(winDriver.findElement(GetPageObjectRead.OR_GetElement(element)));
				Assert.assertTrue(true);
			} else {
				throw new Exception("Element is not found! :" + element);
			}

		} catch (Exception e) {
			log.error(e);
			log.info(e.getMessage() + " " + element);
			throw new CucumberException(e.getMessage());
		}
	}

	@Given("I enter tab key")
	public void enterTabKey() throws AWTException, InterruptedException {
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_TAB);
		Thread.sleep(1000);
	}

//	@Given("I navigate to Customer Creation Page")
//	public void navigateToCustomer() throws AWTException, InterruptedException, FindFailed {
//		Robot robot = new Robot();
//		Screen s = new Screen();
//		robot.keyPress(KeyEvent.VK_DOWN);
//		Thread.sleep(1000);
//		robot.keyPress(KeyEvent.VK_DOWN);
//		Thread.sleep(1000);
//		robot.keyPress(KeyEvent.VK_DOWN);
//		Thread.sleep(1000);
//		robot.keyPress(KeyEvent.VK_DOWN);
//		Thread.sleep(1000);
//		robot.keyPress(KeyEvent.VK_ENTER);
//		Thread.sleep(1000);
//		robot.keyPress(KeyEvent.VK_ENTER);
//		Thread.sleep(3000);
//		//robot.keyPress(KeyEvent.VK_TAB);
//		//Thread.sleep(3000);
////		robot.keyPress(KeyEvent.VK_SHIFT);
////		robot.keyPress(KeyEvent.VK_TAB);
////		robot.keyRelease(KeyEvent.VK_TAB);
////		robot.keyRelease(KeyEvent.VK_SHIFT);
//		
//		//Pattern fileInputTextBox = new Pattern("C:\\Users\\Administrator\\Documents\\Sikuli\\Customer.png");
////		s.wait(fileInputTextBox, 20);
////        s.type(fileInputTextBox, "TEST");	
//	}
	@Given("I double click on the page")
	public void doubleClick() throws AWTException {
		Robot robot = new Robot();
		robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
		robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
		// second click
		robot.mousePress(InputEvent.BUTTON1_DOWN_MASK);
		robot.mouseRelease(InputEvent.BUTTON1_DOWN_MASK);
	}
	@Given("I select value from combobox {string}")
	public void selectValueFromComboBox(String element) throws AWTException, InterruptedException {
		
		Robot robot = new Robot();
		robot.keyPress(KeyEvent.VK_ALT);
		robot.keyPress(KeyEvent.VK_DOWN);
		robot.keyRelease(KeyEvent.VK_ALT);
		robot.keyRelease(KeyEvent.VK_DOWN);
		winDriver.findElement(GetPageObjectRead.OR_GetElement(element)).click();
		

	}
	@Given("I kill the Winium process")
	public void killWiniumProcess() throws IOException, InterruptedException {
		/*
		 * Process process =
		 * Runtime.getRuntime().exec("taskkill /F /IM Winium.Desktop.Driver.exe");
		 * process.waitFor(); process.destroy(); Thread.sleep(10000);
		 */
		winDriver.quit();
		Thread.sleep(10000);
	}
	@Given("I close the application")
	public void closeApplication() throws AWTException, InterruptedException {
		Thread.sleep(3000);
		  Robot robot = new Robot(); 
		  robot.keyPress(KeyEvent.VK_ALT);
		  robot.keyPress(KeyEvent.VK_F4); 
		  robot.keyRelease(KeyEvent.VK_ALT);
		  robot.keyRelease(KeyEvent.VK_F4);
		 
		
		//winDriver.findElement(By.xpath("//*[@LocalizedControlType='header' and @Name='Row 0']")).click();
		
		
	}
	@Given("I select the last row")
	public void selectLastRow() {
		String row = winDriver.findElement(By.id("dataGridView1")).getAttribute("LastChild");
		System.out.println("row....." + row);
		winDriver.findElementByName(row).click();
	}
	@Given("I select the top row in Suppliers")
	public void selectTopRow() throws AWTException {

		winDriver.findElement(GetPageObjectRead.OR_GetElement("headerId")).click();
		winDriver.findElement(GetPageObjectRead.OR_GetElement("cellTopLeftCorner")).click();
		
	}
	@Then("Verify Supplier is added in the list")
	public void verifySupplierAdded() {
		
	}
	@Then("I should see tel number format is correct for text {string} on window at {string}")
	public static void I_should_see_tel_number_format_is_correct_At(String expectedText, String element) {
		try {
			if (expectedText.length() > 1) {
				if (expectedText.substring(0, 2).equals("$$")) {
					log.info("Fetching from HMcontainer!");
					expectedText = HashMapContainer.get(expectedText);
				}

			}
			String actualText = winDriver.findElement(GetPageObjectRead.OR_GetElement(element)).getText();
			// actualText = actualText.substring(11);
			System.out.println(actualText);
			Assert.assertTrue("Expected Format should be ###-###-#### but was "+ expectedText,expectedText.matches("\\d{3}-\\d{3}-\\d{4}"));
			
			// highLightElement(winDriver.findElement(GetPageObjectRead.OR_GetElement(location)));

			StepBase.embedScreenshot();
		} catch (Exception e) {
			log.error(e);
			throw new CucumberException(e.getMessage().substring(0,100), e);
		}
	}


	
	
	
}
