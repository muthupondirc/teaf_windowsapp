package com.TEAF.framework;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.Random;
import java.util.Set;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.imageio.ImageIO;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.ArrayUtils;
import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.json.Json;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.TEAF.Hooks.Hooks;
import com.TEAF.stepDefinitions.CommonStepsWindows;
import com.jayway.jsonpath.JsonPath;

import atu.testrecorder.ATUTestRecorder;
import atu.testrecorder.exceptions.ATUTestRecorderException;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

/**
 * @ScriptName : Utilities
 * @Description : This class contains
 * @Author : Swathin Ratheendren
 * @Creation Date : September 2016 @Modified Date:2019
 */
public class Utilities {

	/**
	 * Method: takeScreenshot Description:
	 * 
	 * @param timeInMilliseconds
	 * @author Swathin Ratheendren
	 * @Creation Date: September 2016 Modified Date:
	 */
	static Logger log = Logger.getLogger(Utilities.class.getName());

	static ATUTestRecorder AtuTestRecorder = null;

	public static WebDriver driver = StepBase.getDriver();

	public static byte[] takeScreenshotByte(WebDriver driver) {
		try {
			byte[] scrFile = null;
			if (System.getProperty("test.disableScreenshotCapture").equalsIgnoreCase("false")) {
				Thread.sleep(1000);
				scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			}
			return scrFile;
		} catch (Exception e) {

			throw new RuntimeException(e);

		}
	}

	public static String takeScreenshot(WebDriver driver) {
		try {
			String SSPath = "";
			if (System.getProperty("test.disableScreenshotCapture").equalsIgnoreCase("false")) {
				Thread.sleep(1000);
				File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
				File directory = new File(String.valueOf("Screenshots"));
				if (!directory.exists()) {
					directory.mkdir();
				}
				SSPath = "Screenshots/" + getRequiredDate(0, "yyyy_MM_dd_hh", null) + "/screenshot_"
						+ getRequiredDate(0, "yyyy_MM_dd_hh_mm_ss", null) + ".png";
				FileUtils.copyFile(scrFile, new File(System.getProperty("user.dir") + "/output/" + SSPath));
			}
			return SSPath;
		} catch (Exception e) {

			throw new RuntimeException(e);
		}
	}
	

    public static byte[] getScreenshot() {
        try {
        	 Robot r = new Robot(); 
        	  
             // It saves screenshot to desired path 
             String path = System.getProperty("user.dir")+"/test.jpg"; 
   
             // Used to get ScreenSize and capture image 
             Rectangle capture =  new Rectangle(Toolkit.getDefaultToolkit().getScreenSize()); 
             BufferedImage Image = r.createScreenCapture(capture); 
             ImageIO.write(Image, "jpg", new File(path)); 
             ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ImageIO.write(Image, "jpg", baos);
             byte[] bytes = baos.toByteArray();
             log.info(">>getScreenshot-byte: "+bytes[0]);
            return bytes;
        } catch (AWTException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } 
        return null;
    }


	public static String get__FormattedTime_byTimeZone_And_TimeOffsetbyMinutes(String timeZone, String timeFormat,
			int timeOffsetfByMinutes) {

		DateFormat tFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

		Date date = new Date();
		String currentTime = tFormat.format(date);
		System.out.println("Current Local Time: " + currentTime);

		tFormat.setTimeZone(TimeZone.getTimeZone(timeZone));
		String Time = tFormat.format(date);
		System.out.println(timeZone + " Time: " + Time);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
		LocalDateTime datetime = LocalDateTime.parse(Time, formatter);

		if (timeOffsetfByMinutes < 0) {
			System.out.println("Subtracting Time");
			datetime = datetime.minusMinutes(-timeOffsetfByMinutes);
		} else {
			System.out.println("Adding Time");
			datetime = datetime.plusMinutes(timeOffsetfByMinutes);
		}

		DateTimeFormatter finalformat = DateTimeFormatter.ofPattern(timeFormat);
		return datetime.format(finalformat);
	}

	public static String startVideoRecorder() throws ATUTestRecorderException, InterruptedException {
		DateFormat dateFormat = new SimpleDateFormat("yy_MM_dd__HH_mm_ss");
		Date date = new Date();

		String RootPath = System.getProperty("user.dir") + "/output/ScreenCaptureVideos/";
		File file = new File(RootPath);
		if (!file.exists()) {
			file.mkdir();
		}
		String VidFileName = "TestVideo-" + dateFormat.format(date);

		AtuTestRecorder = new ATUTestRecorder(RootPath, VidFileName, false);
		AtuTestRecorder.start();
		return RootPath + VidFileName;
	}

	public static void stopVideoRecorder() throws ATUTestRecorderException, InterruptedException {
		if (AtuTestRecorder != null) {
			AtuTestRecorder.stop();
		}
	}

	/**
	 * Method: waitFor Description: Waits for the specified amount of
	 * [timeInMilliseconds].
	 * 
	 * @param timeInMilliseconds
	 * @author Swathin Ratheendren
	 * @Creation Date: September 2016 Modified Date:
	 */
	public void waitFor(final Long timeInMilliseconds) {
		try {
			Thread.sleep(timeInMilliseconds);
		} catch (Exception e) {

			throw new RuntimeException(e);
		}
	}

	public static void waittextToBePresentInElement(String location, String expectedText) {
		try {
			WebDriverWait wb = new WebDriverWait(driver, 50);

			wb.until(ExpectedConditions.textToBePresentInElement(
					driver.findElement(GetPageObjectRead.OR_GetElement(location)), expectedText));
		} catch (Exception e) {

			throw new RuntimeException("expected text");

		}
	}

	public static void scrollToTopOfPage() {
		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			js.executeScript("window.scrollTo(0,-document.body.scrollHeight);");
		} catch (Exception e) {

			throw new RuntimeException(e);

		}
	}

	/**
	 * Method: getRequiredDate Description: This method will give require date
	 * 
	 * @param incrementDateByDays Number by which user want increase date
	 * @param sExpectedDateFormat - User expected date format eg. 9 april 2014 ---
	 *                            dd/MM/yyyy -> 09/04/2015, dd-MM-yyyy -> 09-04-2015
	 * @param timeZoneId          - Time Zone
	 * @author Swathin Ratheendren Creation Date: September 2016 Modified Date:
	 */
	public static String getRequiredDate(int incrementDays, String expectedDateFormat, String timeZoneId) {
		try {
			DateFormat dateFormat;
			Calendar calendar = Calendar.getInstance();
			dateFormat = new SimpleDateFormat(expectedDateFormat);
			if (timeZoneId != null && !timeZoneId.equals(""))
				dateFormat.setTimeZone(TimeZone.getTimeZone(timeZoneId));
			calendar.add(Calendar.DAY_OF_MONTH, incrementDays);
			Date tomorrow = calendar.getTime();
			String formattedDate = dateFormat.format(tomorrow);
			return formattedDate;
		} catch (Exception e) {

			return null;
		}
	}

	/**
	 * Method: copyFileUsingStream Description:
	 * 
	 * @param timeInMilliseconds
	 * @author Swathin Ratheendren Creation Date: September 2016 Modified Date:
	 */
	public void copyFileUsingStream(File source, File dest) throws IOException {
		InputStream is = null;
		OutputStream os = null;
		try {
			is = new FileInputStream(source);
			os = new FileOutputStream(dest);
			byte[] buffer = new byte[1024];
			int length;
			while ((length = is.read(buffer)) > 0) {
				os.write(buffer, 0, length);
			}
		} catch (Exception e) {

		} finally {
			is.close();
			os.close();
		}
	}

	/**
	 * Method: waitForPageLoad Description: timeInSeconds for the specified amount
	 * of [timeInSeconds].
	 * 
	 * @author Swathin Ratheendren Creation Date: September 2016 Modified Date:
	 */
	public void waitForPageLoad() {
		try {
			WebDriverWait wait = new WebDriverWait(StepBase.getDriver(), 180);
			final JavascriptExecutor javascript = (JavascriptExecutor) (StepBase
					.getDriver() instanceof JavascriptExecutor ? StepBase.getDriver() : null);
			/*
			 * wait.until(new ExpectedCondition<Boolean>() {
			 * 
			 * @Override public Boolean apply(WebDriver d) { boolean outcome =
			 * Boolean.parseBoolean(javascript .executeScript("return jQuery.active == 0")
			 * .toString()); return outcome; } });
			 * 
			 */
			wait.until(new ExpectedCondition<Boolean>() {
				// @Override
				public Boolean apply(WebDriver d) {
					return javascript.executeScript("return document.readyState").equals("complete");
				}
			});

			StepBase.getDriver().manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			// objStepBase.getDriver().manage().timeouts().pageLoadTimeout(90,
			// TimeUnit.SECONDS);
		} catch (Exception e) {

			throw new RuntimeException(e);
		}
	}

	/**
	 * Method: waitForAjaxCallComplete Description: timeInSeconds for the specified
	 * amount of [timeInSeconds].
	 * 
	 * @author Swathin Ratheendren Creation Date: September 2016 Modified Date:
	 */
	public void waitForAjaxCallComplete() {
		try {
			WebDriverWait wait = new WebDriverWait(StepBase.getDriver(), 180);
			final JavascriptExecutor javascript = (JavascriptExecutor) (StepBase
					.getDriver() instanceof JavascriptExecutor ? StepBase.getDriver() : null);
			wait.until(new ExpectedCondition<Boolean>() {
				// @Override
				public Boolean apply(WebDriver d) {
					boolean outcome = Boolean
							.parseBoolean(javascript.executeScript("return jQuery.active == 0").toString());
					return outcome;
				}
			});
		} catch (Exception e) {

			throw new RuntimeException(e);
		}
	}

	public static void resizeImage(File inputImagePath, File outputImagePath, int scaledWidth, int scaledHeight)
			throws IOException {
		// reads input image
		BufferedImage inputImage = ImageIO.read(inputImagePath);

		// creates output image
		BufferedImage outputImage = new BufferedImage(scaledWidth, scaledHeight, inputImage.getType());

		// scales the input image to the output image
		Graphics2D g2d = outputImage.createGraphics();
		g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
		g2d.dispose();

		log.info("Resize completed : " + outputImagePath);

		/*
		 * // extracts extension of output file String formatName =
		 * outputImagePath.substring(outputImagePath.lastIndexOf(".") + 1);
		 */

		// writes to output file
		ImageIO.write(outputImage, "png", outputImagePath);
	}

	/**
	 * Method: ImageComparison Description: timeInSeconds for the specified amount
	 * of [timeInSeconds].
	 * 
	 * @author Swathin Ratheendren Creation Date: September 2016 Modified Date:
	 */
	public static float compareImage(File ActualImage, File BaseLinedImage) {
		float percentage = 0;
		try {
			// take buffer data from both image files //
			BufferedImage biA = ImageIO.read(ActualImage);
			DataBuffer dbA = biA.getData().getDataBuffer();
			int sizeA = dbA.getSize();
			BufferedImage biB = ImageIO.read(BaseLinedImage);
			DataBuffer dbB = biB.getData().getDataBuffer();

			int sizeB = dbB.getSize();
			int count = 0;
			// compare data-buffer objects //
			if (sizeA == sizeB) {
				for (int i = 0; i < sizeA; i++) {
					if (dbA.getElem(i) == dbB.getElem(i)) {
						count = count + 1;
					}
				}
				percentage = (count * 100) / sizeA;
				percentage = 100 - percentage;
			} else {
				log.info("Both the images are not of same size");
				throw new Exception("Both the images are not of same size");
			}
		} catch (Exception e) {

		}
		return percentage;
	}

	public static BufferedImage getDifferenceImage(File ActualImage, File BaseLinedImage) throws Exception {
		BufferedImage img1 = ImageIO.read(ActualImage);
		// DataBuffer dbA = img1.getData().getDataBuffer();
		BufferedImage img2 = ImageIO.read(BaseLinedImage);
		// DataBuffer dbB = img2.getData().getDataBuffer();

		// convert images to pixel arrays...
		final int w = img1.getWidth(), h = img1.getHeight(), highlight = Color.RED.getRGB();
		final int[] p1 = img1.getRGB(0, 0, w, h, null, 0, w);
		final int[] p2 = img2.getRGB(0, 0, w, h, null, 0, w);
		// compare pixel by pixel of img1 to img2. Highlight img1's pixels which are
		// different.
		for (int i = 0; i < p1.length; i++) {
			if (p1[i] != p2[i]) {
				p1[i] = highlight;
			}
		}
		// save img1's pixels to a new BufferedImage, and return it...
		// (May require TYPE_INT_ARGB)
		final BufferedImage out = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		out.setRGB(0, 0, w, h, p1, 0, w);
		return out;
	}

	/*
	 * public static void main(String[] args) throws Exception { File f1 = new
	 * File("C:/Users/user/Desktop/image1.png"); File f2 = new
	 * File("C:/Users/user/Desktop/image2.png"); log.info(compareImage(f1,f2));
	 * 
	 * BufferedImage bufferedImage = getDifferenceImage(f1,f2);
	 * 
	 * File outputfile = new
	 * File("C:/Users/user/Desktop/output_differenceimage.png");
	 * ImageIO.write(bufferedImage, "jpg", outputfile);
	 * 
	 * }
	 */

	public static String getDBData(String DBUrl, String DBName, String DBUserName, String DBPassword,
			String QueryStatement, int columnIndex) throws SQLException {
		String data = null;

		String connectionString = "jdbc:sqlserver://" + DBUrl + ";" + "database=" + DBName + ";" + "user=" + DBUserName
				+ ";" + "password=" + DBPassword + ";" + "encrypt=true;" + "trustServerCertificate=true;"
				+ "hostNameInCertificate=*.database.windows.net;" + "loginTimeout=30;";
		Connection connObj = DriverManager.getConnection(connectionString);

		Statement stmtObj = connObj.createStatement();
		ResultSet resObj = stmtObj.executeQuery(QueryStatement);
		while (resObj.next()) {
			data = resObj.getString(columnIndex);
			log.info(data);
		}
		return data;
	}

	public static String getDBData(String DBUrl, String DBName, String DBUserName, String DBPassword,
			String QueryStatement, String columnName) throws SQLException {
		String data = null;

		String connectionString = "jdbc:sqlserver://" + DBUrl + ";" + "database=" + DBName + ";" + "user=" + DBUserName
				+ ";" + "password=" + DBPassword + ";" + "encrypt=true;" + "trustServerCertificate=true;"
				+ "hostNameInCertificate=*.database.windows.net;" + "loginTimeout=30;";
		Connection connObj = DriverManager.getConnection(connectionString);

		Statement stmtObj = connObj.createStatement();
		ResultSet resObj = stmtObj.executeQuery(QueryStatement);
		if (columnName != null) {
			while (resObj.next()) {
				data = resObj.getString(columnName);
				log.info(data);
			}
		}
		return data;
	}

	public static void ExecuteQuery(String DBUrl, String DBName, String DBUserName, String DBPassword,
			String QueryStatement) throws SQLException {
		// String data=null;

		String connectionString = "jdbc:sqlserver://" + DBUrl + ";" + "database=" + DBName + ";" + "user=" + DBUserName
				+ ";" + "password=" + DBPassword + ";" + "encrypt=true;" + "trustServerCertificate=true;"
				+ "hostNameInCertificate=*.database.windows.net;" + "loginTimeout=30;";
		Connection connObj = DriverManager.getConnection(connectionString);

		Statement stmtObj = connObj.createStatement();
		stmtObj.execute(QueryStatement);
	}

	/* Generates Random String */
	public static String getRandomString(int stringSize) {
		// 'E' changed to 'a'
		byte[] stringarray1 = { 'a' };
		byte[] stringarray2 = new byte[stringSize - 1];
		for (int strsizeCounter = 0; strsizeCounter < stringSize - 1; strsizeCounter++) {
			stringarray2[strsizeCounter] = (byte) rand('a', 'z');
		}
		String temprandom = new String(ArrayUtils.addAll(stringarray1, stringarray2));
		// log.debug(temprandom);
		return temprandom;
	}

	public static Random rn = new Random();

	public static int rand(int lo, int hi) {
		int n = hi - lo + 1;
		if (n > 9)
			n = 9;
		int i = rn.nextInt() % n;
		if (i < 0)
			i = -i;
		return lo + i;
	}

	/* Generates Random Numbers */
	public static String getRandomNum(int stringSize) {
		int[] strnumarray = new int[stringSize];
		StringBuffer buf = new StringBuffer();
		for (int strsizeCounter = 0; strsizeCounter < stringSize; strsizeCounter++) {
			strnumarray[strsizeCounter] = rand(0, 9);
			if (strnumarray[0] == 0)
				strnumarray[0] = 1;
			buf.append(strnumarray[strsizeCounter]);
		}
		String temprandom = buf.toString();
		return temprandom;
	}

	/* Generates Random AlphaNumeric Characters */
	public static String getRandomAlphaNumeric(int size) {
		Random rand = new Random();
		String[] charset = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r",
				"s", "t", "u", "v", "w", "x", "y", "z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		String[] numset = { "0", "1", "2", "3", "4", "5", "6", "7", "8", "9" };
		StringBuffer sb = new StringBuffer();
		for (int n = 0; n < size - 1; n++) {
			sb = sb.append(charset[rand.nextInt(30)]);
		}

		sb = sb.append(numset[rand.nextInt(9)]);

		return (sb.toString());
	}

	/* Generate Random Email */
	public static String ReEnterEmail;

	public static String GetRandomEmail(int size) {

		String Value = "email";
		if (Value.equalsIgnoreCase("email")) {
			Random rand = new Random();
			String[] charset = { "a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q",
					"r", "s", "t", "u", "v", "w", "x", "y", "z" };
			StringBuffer sb = new StringBuffer();
			for (int n = 0; n < size; n++) {
				sb = sb.append(charset[rand.nextInt(26)]);
			}
			// if(flag==1)
			{
				StringBuffer sb1 = sb.append("@datagenerator.com");
				ReEnterEmail = sb1.toString();
				return (sb1.toString());
			} // end of if
		} else
			return (ReEnterEmail);
	}

	public static void deleteZipFiles(String desFN) {
		try {
			String zipName = System.getProperty("user.dir") + "/" + desFN + ".zip";
			String folderName = System.getProperty("user.dir") + "/" + desFN + ".zip";

			FileUtils.forceDelete(new File(zipName));
			FileUtils.forceDelete(new File(folderName));

		} catch (Exception e) {
			// log.info("No zip folder found to delete" + desFN);
			//

		}
	}

	public static void deleteFiles(String desFN) {
		try {
			String zipName = System.getProperty("user.dir") + "/" + desFN;
			FileUtils.deleteDirectory(new File(zipName));

		} catch (Exception e) {
			// TODO Auto-generated catch block
			// log.info("No dir found to delete");
			//

		}
	}

	public static void reportstoZipFile(String srcfolder, String desFN) throws Exception {

		try {

			// Use the following paths for windows
			String folderToZip = System.getProperty("user.dir") + "/" + srcfolder;
			String zipName = System.getProperty("user.dir") + "/" + desFN + ".zip";

			File f = new File(folderToZip);
			if (f.isDirectory()) {

				final Path sourceFolderPath = Paths.get(folderToZip);
				Path zipPath = Paths.get(zipName);
				final ZipOutputStream zos = new ZipOutputStream(new FileOutputStream(zipPath.toFile()));
				Files.walkFileTree(sourceFolderPath, new SimpleFileVisitor<Path>() {
					public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
						zos.putNextEntry(new ZipEntry(sourceFolderPath.relativize(file).toString()));
						Files.copy(file, zos);
						zos.closeEntry();
						return FileVisitResult.CONTINUE;
					}
				});
				zos.close();
			}
		} catch (Exception e) {
			//
		}

	}

	public static void auto_generation_Email(String mailId, String ccMail) throws Exception {

		// Create object of Property file
		Properties props = new Properties();
		props.put("mail.smtp.host", System.getProperty("email.smtp.server"));
		props.put("mail.smtp.socketFactory.port", System.getProperty("email.smtp.socketPortNumber"));
		props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.port", System.getProperty("email.smtp.portNumber"));
		props.put("mail.smtp.starttls.enable", "true");

		// This will handle the complete authentication
		Session session = Session.getInstance(props, new javax.mail.Authenticator() {
			protected PasswordAuthentication getPasswordAuthentication() {
				return new PasswordAuthentication(System.getProperty("email.user.emailId"),
						System.getProperty("email.user.password"));
			}
		});

		try {

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(System.getProperty("email.user.emailAddress")));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mailId));
			message.addRecipients(Message.RecipientType.CC, InternetAddress.parse(ccMail));
			String lDate = LocalDate.now().toString();
			String lTime = LocalTime.now().toString();

			Collection<String> values = Hooks.scenarioStatus.values();
			String flag;
			int failCount = 0;
			int passCount = 0;
			int skipCount = 0;
			for (String x : values) {
				if (x.equalsIgnoreCase("Failed")) {
					failCount++;
				} else if (x.equalsIgnoreCase("Passed")) {
					passCount++;
				} else {
					skipCount++;
				}
			}
			if (values.contains("FAILED") || values.contains("SKIPPED")) {
				flag = "Build Failed";
			} else {
				flag = "Build Passed";
			}

			String subject = null;
			if (System.getProperty("email.subject").length() > 2) {
				subject = System.getProperty("email.subject") + ":" + " on " + lDate + ": " + lTime + " :" + flag;
			} else {
				subject = "RC TEAF Execution Report " + ":" + " on " + lDate + ": " + lTime + " :" + flag;

			}

			message.setSubject(subject);
			MimeMultipart multipart = new MimeMultipart("related");

			BodyPart messageBodyPart1 = new MimeBodyPart();
			StringBuffer ScenarioTable = new StringBuffer();
			LinkedHashMap<String, String> mp = new LinkedHashMap<String, String>();
			mp.putAll(Hooks.scenarioStatus);
			Set<Entry<String, String>> entrySet = mp.entrySet();

			for (Entry<String, String> entry : entrySet) {

				if (entry.getValue().equalsIgnoreCase("passed")) {
					ScenarioTable.append("<TR ALIGN='CENTER' bgcolor ='#f2f2f2'><TD>" + entry.getKey()
							+ "</TD><TD bgcolor= '#419c4d' >" + entry.getValue() + "</TD> " + "</TR>");
				} else {
					ScenarioTable.append("<TR ALIGN='CENTER' bgcolor ='#f2f2f2'><TD>" + entry.getKey()
							+ "</TD><TD bgcolor= '#d63a29' >" + entry.getValue() + "</TD> " + "</TR>");
				}

			}

			String messageBody = null;
			if (System.getProperty("email.messageBody").length() > 2) {
				messageBody = System.getProperty("email.messageBody");
			} else {
				messageBody = "";

			}

			String signature = null;
			if (System.getProperty("email.signature").length() > 2) {
				signature = System.getProperty("email.signature");
			} else {
				signature = "RC QA";

			}

			File f = new File(System.getProperty("user.dir") + "/src/test/java/com/TestResults/cucumber-report/"
					+ System.getProperty("test.jsonFileName") + ".json");
			List<String> listSteps = JsonPath.read(f, "$..steps[*].result.status");
			int stepPassed = 0;
			int stepFailed = 0;
			int stepSkipped = 0;
			for (String x : listSteps) {
				if (x.toLowerCase().contains("pass")) {
					stepPassed++;
				} else if (x.toLowerCase().contains("fail")) {
					stepFailed++;
				} else if (x.toLowerCase().contains("skip")) {
					stepSkipped++;
				}
			}

			// Set the body of email
			String htmlText = "<H3><img src=\"cid:image\">   Script Execution Summary | Email Report </H>" + "<H4></H4>"
					+ "<TABLE WIDTH='75%' CELLPADDING='8' CELLSPACING='1'>"
					+ "<TR> <TH bgcolor = '#a8a7a7' COLSPAN='8'><H2>Test Execution Summary</H2></TH></TR><TR><TH>Total Test Steps: </TH><TH>Total Test Steps Passed: </TH><TH>Total Test Steps Failed: </TH><TH>Total Test Steps Skipped: </TH><TH>Total Test Cases: </TH><TH>Total Test cases Passed: </TH><TH>Total Test cases Failed: </TH><TH>Total Test cases Skipped: </TH>"
					+ "</TR>" + "<TR ALIGN='CENTER' bgcolor ='#f2f2f2'>" + "<TD>" + listSteps.size() + "</TD>"
					+ "<TD bgcolor= '#419c4d'>" + stepPassed + "</TD>" + "<TD  bgcolor= '#d63a29'>" + stepFailed
					+ "</TD>" + "<TD bgcolor= '#6bbbc7'>" + stepSkipped + "</TD>"

					+ "<TD>" + values.size() + "</TD><TD bgcolor= '#419c4d' >" + passCount
					+ "</TD><TD bgcolor= '#d63a29' >" + failCount + "</TD><TD bgcolor= '#6bbbc7' >" + skipCount
					+ "</TD>" + "</TR>" + "</TABLE><H4>" + messageBody + "<BR>" + "<BR>"
					+ "<p> Please find attached a detailed Test Automation execution report using RC TEAF with this email\r\n"
					+ " </p>" + "</H4> " + "\n" + "\n" + "<H4>Test Scenarios executed:</H4>"
					+ "<TABLE WIDTH='60%' CELLPADDING='4' CELLSPACING='1'>"
					+ "<TR ALIGN='CENTER' bgcolor ='#3767B6'><TD color: 'white'> Scenario Name </TD><TD bgcolor ='#3767B6' color: 'white'>Result</TD></TR>"
					+ ScenarioTable + "</TABLE>" + "\n" + "\n" +

					"Thanks, " + "\n" + "\n" + signature;
			messageBodyPart1.setContent(htmlText, "text/html");
			// add it
			multipart.addBodyPart(messageBodyPart1);

			// second part (the image)
			messageBodyPart1 = new MimeBodyPart();
			DataSource fds = new FileDataSource(
					System.getProperty("user.dir") + "/src/test/java/com/Resources/teaf.jpeg");

			messageBodyPart1.setDataHandler(new DataHandler(fds));
			messageBodyPart1.setHeader("Content-ID", "<image>");

			// add image to the multipart
			multipart.addBodyPart(messageBodyPart1);

			// Create another object to add another content
			MimeBodyPart messageBodyPart2 = new MimeBodyPart();
			MimeBodyPart messageBodyPart3 = new MimeBodyPart();

			// Mention the file which you want to send
			String fEN = System.getProperty("user.dir") + "/" + System.getProperty("reports.FolderName") + ".zip";
			String fUI = System.getProperty("user.dir") + "/TestExecution_UIReports.zip";

			multipart.addBodyPart(messageBodyPart1);

			if (java.nio.file.Files.exists(Paths.get(fEN), LinkOption.NOFOLLOW_LINKS)) {
				DataSource src = new FileDataSource(fEN);
				// set the handler
				messageBodyPart2.setDataHandler(new DataHandler(src));
				// set the file
				messageBodyPart2.setFileName(fEN);
				multipart.addBodyPart(messageBodyPart2);
			}

			if (java.nio.file.Files.exists(Paths.get(fUI), LinkOption.NOFOLLOW_LINKS)) {
				DataSource source1 = new FileDataSource(fUI);

				// set the handler
				messageBodyPart3.setDataHandler(new DataHandler(source1));
				// set the file
				messageBodyPart3.setFileName(fUI);
				multipart.addBodyPart(messageBodyPart3);

			}
			message.setContent(multipart);
			// finally send the email
			Transport.send(message);

			log.info("\n =====Reports Sent through Email from " + System.getProperty("email.user.emailAddress")
					+ "=====");

		} catch (MessagingException e) {
			e.printStackTrace();
		}
	}

	public static void testStatusToastMessage(String message) throws InterruptedException {

		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// Check for jQuery on the page, add it if need be
			js.executeScript("if (!window.jQuery) {"
					+ "var jquery = document.createElement('script'); jquery.type = 'text/javascript';"
					+ "jquery.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js';"
					+ "document.getElementsByTagName('head')[0].appendChild(jquery);" + "}");
			Thread.sleep(1000);
			// Use jQuery to add jquery-growl to the page
			js.executeScript("$.getScript('https://the-internet.herokuapp.com/js/vendor/jquery.growl.js')");
			// js.executeScript("$.getScript('/Users/NaveenKhunteta/Documents/workspace/Test/src/testcases/jquery.growl.js')");

			// Use jQuery to add jquery-growl styles to the page
			js.executeScript("$('head').append('<link id=\"scenariotoast\" rel=\"stylesheet\" "
					+ "href=\"https://the-internet.herokuapp.com/css/jquery.growl.css\" " + "type=\"text/css\" />');");
			Thread.sleep(1000);
			// jquery-growl w/ no frills
			js.executeScript("$.growl({ title: 'Scenario', message: '" + message + "' });");

			Thread.sleep(3000);

			js.executeScript("var element = document.getElementById('scenariotoast');"
					+ "element.parentNode.removeChild(element);");
			Thread.sleep(1000);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());

		}

	}

	public static void testStatusFailToastMessage(String message) throws InterruptedException {

		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// Check for jQuery on the page, add it if need be
			js.executeScript("if (!window.jQuery) {"
					+ "var jquery = document.createElement('script'); jquery.type = 'text/javascript';"
					+ "jquery.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js';"
					+ "document.getElementsByTagName('head')[0].appendChild(jquery);" + "}");
			Thread.sleep(1000);
			// Use jQuery to add jquery-growl to the page
			js.executeScript("$.getScript('https://the-internet.herokuapp.com/js/vendor/jquery.growl.js')");
			// js.executeScript("$.getScript('/Users/NaveenKhunteta/Documents/workspace/Test/src/testcases/jquery.growl.js')");

			// Use jQuery to add jquery-growl styles to the page
			js.executeScript("$('head').append('<link id=\"failtoast\" rel=\"stylesheet\" "
					+ "href=\"https://the-internet.herokuapp.com/css/jquery.growl.css\" " + "type=\"text/css\" />');");
			Thread.sleep(1000);
			// jquery-growl w/ no frills
			js.executeScript("$.growl.error({ title: 'Failed', message: '" + message + "' });");

			Thread.sleep(1000);

			js.executeScript(
					"var element = document.getElementById('failtoast');" + "element.parentNode.removeChild(element);");
			Thread.sleep(1000);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());

		}

	}

	public static void testStatusToastPass(String message) throws InterruptedException {

		try {
			JavascriptExecutor js = (JavascriptExecutor) driver;
			// Check for jQuery on the page, add it if need be
			js.executeScript("if (!window.jQuery) {"
					+ "var jquery = document.createElement('script'); jquery.type = 'text/javascript';"
					+ "jquery.src = 'https://ajax.googleapis.com/ajax/libs/jquery/2.0.2/jquery.min.js';"
					+ "document.getElementsByTagName('head')[0].appendChild(jquery);" + "}");
			Thread.sleep(1000);
			// Use jQuery to add jquery-growl to the page
			js.executeScript("$.getScript('https://the-internet.herokuapp.com/js/vendor/jquery.growl.js')");
			// js.executeScript("$.getScript('/Users/NaveenKhunteta/Documents/workspace/Test/src/testcases/jquery.growl.js')");

			// Use jQuery to add jquery-growl styles to the page
			js.executeScript("$('head').append('<link id=\"passtoast\" rel=\"stylesheet\" "
					+ "href=\"https://the-internet.herokuapp.com/css/jquery.growl.css\" " + "type=\"text/css\" />');");
			Thread.sleep(1000);
			// jquery-growl w/ no frills
			js.executeScript("$.growl.notice({ title: 'Passed', message: '" + message + "' });");
			Thread.sleep(3000);

			js.executeScript(
					"var element = document.getElementById('passtoast');" + "element.parentNode.removeChild(element);");
			Thread.sleep(1000);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			log.debug(e.getMessage());

		}

	}

}
