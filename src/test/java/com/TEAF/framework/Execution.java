package com.TEAF.framework;

import java.util.Collection;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import com.TEAF.Hooks.Hooks;
import com.TEAF.TestFiles.Runner.TestRunner_1;
import com.TEAF.stepDefinitions.GalenStep;
import com.galenframework.config.GalenConfig;
import com.galenframework.config.GalenProperty;

public class Execution {

	static Logger log = Logger.getLogger(Execution.class.getName());
	static Process WiniumProcess;
	static String platform;
	static String browser;

	public static void setup(String Platform, String Browser, String URL, String appType, String winAppLocation,
			String TestName, String jsonFileName) {
		try {

			PropertyConfigurator.configure(
					System.getProperty("user.dir") + "/src/test/java/com/TEAF/TestFiles/Config/log4j.properties");

			// LoadConfigurations
			TestConfig.LoadAllConfig();

			platform = System.getProperty("PlatformName", Platform);
			browser = System.getProperty("BrowserName", Browser);
			appType = System.getProperty("AppType", appType);
			winAppLocation = System.getProperty("AppLocation", winAppLocation);
			URL = System.getProperty("URL", URL);

			System.setProperty("test.platformName", Platform);
			System.setProperty("test.browserName", Browser);
			System.setProperty("test.jsonFileName", jsonFileName);

			System.setProperty("test.appType", appType);

			if (winAppLocation != null) {
				log.info("Win App Path: "+winAppLocation);
				System.setProperty("test.winDesktopApp", winAppLocation);
			}
			if (URL != null) {
				System.setProperty("test.appUrl", URL);

			}
			System.setProperty("test.TestName", TestName);

			// Report configurations
			String reportPath = GenerateCustomReport.reportPath;

			if (System.getProperty("test.disableExtentReport").equalsIgnoreCase("false")) {
				System.setProperty("screenshot.dir", reportPath + "/Screenshots/");
				System.setProperty("extent.reporter.html.start", "true");
				System.setProperty("extent.reporter.spark.start", "true");
				System.setProperty("extent.reporter.html.out", reportPath + "/ExtentTestReport.html");
				System.setProperty("extent.reporter.spark.out", reportPath + "/");
				// System.setProperty("extent.reporter.logger.start", "true");
				// System.setProperty("extent.reporter.html.config","src/test/java/com/Resources/extent-config.xml");
				// System.setProperty("extent.reporter.logger.out", "output/LoggerOutput/");
				// System.setProperty("extent.reporter.klov.start", "true");
				// System.setProperty("extent.reporter.klov.config", "klov.properties");
			}
			// Galen Property
			GalenConfig.getConfig().setProperty(GalenProperty.SCREENSHOT_FULLPAGE, "true");

			// ScreenVideoCapture
			if (System.getProperty("test.disableScreenVideoCapture").equalsIgnoreCase("false")) {
				Utilities.startVideoRecorder();
			}
			// SessionSetup
			log.info("Platform: " + Platform);
			log.info("Browser : " + Browser);
			log.info("AppType : " + appType);
			if (Platform.equals("desktop") && !Browser.equals("REST Service") && !Browser.equals("DB Service")) {
				StepBase.setUp(Platform, Browser);
			} else if (Platform.equals("mobile") && !Browser.equals("REST Service") && !Browser.equals("DB Service")) {
				// StepBase.appiumStart(); //Appium server Should be installed via node js for
				// this to work
				StepBase.setUp(Platform, Browser);
			} else if(Platform.equals("desktop") && appType.equals("windowsapp")){
				log.info("Running Tests on Windows Application");
				WiniumProcess = Runtime.getRuntime().exec(
						System.getProperty("user.dir") + "/src/test/java/com/Resources/Winium.Desktop.Driver.exe");
			}
				else {
				System.out.println("Enter valid platform choice: desktop / android / ios");
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void tearDown(String successFulRecipients, String failureRecipients, String successFulCCRecipients,
			String failureCCRecipients) {

		try {

			if (System.getProperty("test.disableGalenReport").equalsIgnoreCase("false")) {
				GalenStep.generateUIReport();
			}

			if (platform.equals("desktop") && !browser.equals("REST Service") && !browser.equals("DB Service")) {
				StepBase.tearDown();
			} else if (platform.equals("mobile") && !browser.equals("REST Service") && !browser.equals("DB Service")) {
				StepBase.tearDown();
			} else {
			}

			Collection<String> values = Hooks.scenarioStatus.values();
			String toMailId = null;
			String ccMailId = null;
			boolean flag = false;
			if ((values.contains("FAILED") || values.contains("SKIPPED"))
					&& !(Hooks.code == 520 || Hooks.code == 521)) {
				flag = true;
			}

			log.info("Failure Present: " + flag);
			if (flag == true) {
				// Mailing only internal team to review the failures
				toMailId = successFulRecipients;
				ccMailId = successFulCCRecipients;
			} else {
				// Mailing everyone as results look good
				toMailId = failureRecipients;
				ccMailId = failureCCRecipients;
			}

			ExcludeHook.removeBeforeAfterHookForPassedStatus(System.getProperty("test.jsonFileName"));

			// Generation of Default Cucumber Reports
			if (!System.getProperty("test.disableCucumberReport").equalsIgnoreCase("true")) {
				// Cucumber Report Generation

				GenerateCustomReport.generateCustomeReport(System.getProperty("test.browserName"),
						System.getProperty("test.platformName"), System.getProperty("test.jsonFileName"));
				HashMapContainer.ClearHM();
			}

			String zipReportFiles = ZipFiles.zipReportFiles(GenerateCustomReport.reportPath,
					System.getProperty("reports.FolderName"));

			if (System.getProperty("test.generateFTPTransfer").equalsIgnoreCase("true")) {
				try {

					UploadReportsToFTPServer.connectToFTPandTransferFiles("ip", "user", "password", zipReportFiles,
							"reports.zip", "/user25");
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			// Prepare reports for Email
			if (System.getProperty("test.generateEmail").equalsIgnoreCase("true")) {
				String toMail = System.getProperty("ToMailID", toMailId);
				String ccMail = System.getProperty("CCMailID", ccMailId);
				log.info("To email id : " + toMail);
				log.info("Cc email id : " + ccMail);
				try {
					Utilities.auto_generation_Email(toMail, ccMail);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			try {
				Utilities.deleteZipFiles(System.getProperty("reports.FolderName"));
				Utilities.deleteZipFiles("TestExecution_UIReports");
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
}
