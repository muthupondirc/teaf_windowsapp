package com.TEAF.framework;

import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

public class DB_UserDefined {
	
	private List<String> resultInList ;
	
	private Connection con;

	public Connection getCon() {
		return con;
	}

	public List<String> getResultInList() {
		return resultInList;
	}

	public void setResultInList(List<String> resultInList) {
		this.resultInList = resultInList;
	}

	public void setCon(Connection con) {
		this.con = con;
	}

	private ResultSet rs;

	public ResultSet getRs() {
		return rs;
	}

	public void setRs(ResultSet rs) {
		this.rs = rs;
	}

}
