package com.TEAF.framework;

import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.text.PDFTextStripperByArea;
import org.junit.Assert;

import java.io.File;
import java.io.IOException;

public class ReadPdf {

	private static File getLatestFilefromDir(String dirPath) {
		File dir = new File(dirPath);
		File[] files = dir.listFiles();
		if (files == null || files.length == 0) {
			return null;
		}

		File lastModifiedFile = files[0];
		for (int i = 1; i < files.length; i++) {
			if (lastModifiedFile.lastModified() < files[i].lastModified()) {
				lastModifiedFile = files[i];
			}
		}
		return lastModifiedFile;
	}

	public static void readlines(String foldername, int lineNum, String verifyValue) throws IOException {

		File f = new File(System.getProperty("user.dir") + "\\src\\test\\java\\"+ foldername);
		File latestfile = getLatestFilefromDir(f.getAbsolutePath());

		PDDocument document = PDDocument.load(latestfile);

		document.getClass();

		if (!document.isEncrypted()) {

			PDFTextStripperByArea stripper = new PDFTextStripperByArea();
			stripper.setSortByPosition(true);

			PDFTextStripper tStripper = new PDFTextStripper();

			String pdfFileInText = tStripper.getText(document);
			// System.out.println("Text:" + st);

			// split by whitespace
			String lines[] = pdfFileInText.split("\\r?\\n");
			System.out.println("=========================PDF Validations=====================");
			for (String line : lines) {
				System.out.println(line);

			}
			Assert.assertEquals(verifyValue, lines[lineNum]);
		}

	}

	
}