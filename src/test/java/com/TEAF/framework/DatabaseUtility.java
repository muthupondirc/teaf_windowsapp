package com.TEAF.framework;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DatabaseUtility {

	static DB_UserDefined db = new DB_UserDefined();

	public static Connection connectToSqlDB(String driver, String server, String username, String password)
			throws Exception {

		try {
			Class.forName(driver);
			Connection con = DriverManager.getConnection(server, username, password);
			db.setCon(con);
			return con;
		} catch (Exception e) {
			System.out.println(e);
			throw new Exception("Connection Issue");
		}
	}

	public static void executeQuery(String query) throws Throwable {
		Statement stmt = db.getCon().createStatement();
		ResultSet rs = stmt.executeQuery(query);
		db.setRs(rs);
	}

	public static void getResultSet(String... args) throws Throwable {
		ResultSet rs = db.getRs();
		List<String> resultList = new ArrayList<String>();
		while (rs.next()) {
			String result = null;
			for (int i = 0; i < args.length; i++) {
				result = rs.getString(args[i]) + " | ";
				resultList.add(result);
			}
		}
		db.setResultInList(resultList);
	}

	public static void closeSQLDBConnection() throws SQLException {
		db.getCon().close();
	}

	public static void connectMicrosoftSQLServerWithWindowsAuthentication(String driver, String dburl)
			throws ClassNotFoundException, SQLException {
		Class.forName(driver);
		String dbURL = dburl;
		Connection con = DriverManager.getConnection(dbURL);
		db.setCon(con);
	}

	public static void main(String[] args) throws Throwable {

		connectToSqlDB("com.mysql.jdbc.Driver", "jdbc:mysql://localhost:3306/classicmodels,user", "root", "root");
		executeQuery("select * from orders");
		getResultSet("orderNumber", "orderDate");
		closeSQLDBConnection();
	}

}
