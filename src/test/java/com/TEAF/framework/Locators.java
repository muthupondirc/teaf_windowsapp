package com.TEAF.framework;

public class Locators{

	String locatorName;
	String locatorType;
	String locatorValue;
	
	public Locators(String locatorName, String locatorType, String locatorValue) {
		super();
		this.locatorName = locatorName;
		this.locatorType = locatorType;
		this.locatorValue = locatorValue;
	}
	public String getLocatorName() {
		return locatorName;
	}
	public void setLocatorName(String locatorName) {
		this.locatorName = locatorName;
	}
	public String getLocatorType() {
		return locatorType;
	}
	public void setLocatorType(String locatorType) {
		this.locatorType = locatorType;
	}
	public String getLocatorValue() {
		return locatorValue;
	}
	public void setLocatorValue(String locatorValue) {
		this.locatorValue = locatorValue;
	}
	@Override
	public String toString() {
		return "Locators [locatorName=" + locatorName + ", locatorType=" + locatorType + ", locatorValue="
				+ locatorValue + "]";
	}

}

	
	
	


