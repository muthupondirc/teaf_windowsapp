package com.TEAF.framework;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Attributes;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;

public class LocatorGenerator {

	static String BaseUrl = "https://www.google.com";
	// static String BaseUrl="https://www.amazon.in/";
	static WebDriver driver = null;
	static String locatorExcelPath = System.getProperty("user.dir")
			+ "\\src\\test\\java\\com\\TEAF\\TestFiles\\PageObjects\\WebApp\\LocatorExcel.xlsx";
	static Locators loc;
	static Logger log = Logger.getLogger(LocatorGenerator.class.getName());
	static int WriteCount = 1;
	static String appName;
	static String currentUrl;
	static String parentTagAttr;
	static String parentTagKey;
	static String page;
	static String section;
	


	public static void createExcel(String AppName, String sec, String fileName, String Url) throws Exception {
		try {
			appName = AppName;
			currentUrl = Url;
			page = fileName;
			section = sec;

			locatorExcelPath = System.getProperty("user.dir") + "\\src\\test\\java\\com\\TEAF\\TestFiles\\PageObjects\\"
					+ AppName + "\\" + fileName + ".xlsx";
			// log.info(locatorExcelPath);
			log.debug(locatorExcelPath);
			log.info(locatorExcelPath);
			File excelFile = new File(locatorExcelPath);
		//	log.info(locatorExcelPath + " file is exist ..?  : " + excelFile.exists());
			log.info(locatorExcelPath + " file is exist ..?  : " + excelFile.exists());
		} catch (Exception e) {
			// TODO Auto-generated catch block
			//e.printStackTrace();
			log.error(e);
			throw new Exception();
		}
	}

	public static String savePage(String pageUrl) throws IOException {
		System.setProperty("webdriver.chrome.driver",
				"D:\\Workspaces\\workspace_Labs\\TEAF_Tests\\src\\test\\java\\com\\Resources\\chromedriver.exe");
		WebDriver driver = new ChromeDriver();
		driver.get(BaseUrl);
		String pageSource = driver.getPageSource();
		return pageSource;
	}

	public static void getAllLinksInSection(Elements anchorElements) throws IOException {
		List<Element> anchors = anchorElements;
		// All a tags with linkText
		int a = 0, b = 0, c = 0, d = 0, e = 0;
		List<Locators> locators = new ArrayList<Locators>();
		int id = 0;

		for (int i = 0; i < anchors.size(); i++) {

			Element link = anchors.get(i);
			// log.info(x +" All LInks: "+ allLinks.html());
			Attributes att = link.attributes();
			List<Attribute> attributeList = att.asList();
			String aTag = null;

			for (Attribute at : attributeList) {
				// Link with Text
				if (link.attr("id").length() > 2) {
					Pattern pattern = Pattern.compile("[0-9]+");
					Matcher matcher = pattern.matcher(link.attr("id"));
					String match = null;
					// Find all matches
					while (matcher.find()) {
						// Get the matching string
						match = matcher.group();
					}
					if (match == null) {
						aTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//a[@" + "id" + "='"
								+ link.attr("id") + "']";
						log.info("Link Element with id  =  " + aTag);
					//	log.info("Link Element with id  =  " + aTag);
						id++;
						break;
					}

				}
				if (link.hasText()) {
					if (link.ownText().length() > 2) {
						a++;

						aTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//a[contains(text(),'"
								+ link.ownText() + "')]";
						log.info("Link Element with text  =  " + aTag);
						//log.info("Link Element with text  =  " + aTag);
						break;
					}
				}
				// else {
//						e++;
//					//	Elements elementsContainingOwnText = link.getElementsContainingOwnText(link.text());
//						aTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//a/" + link.child(0).tagName()
//								+ "[contains(text(),'" + link.text() + "')]";
//						// log.info(e+" Element with Child Tag with Text: " +aTag);
//						break;
//					}
				// Link with ID or Title or altText
				else if (at.getValue().length() > 1) {
					if (at.getKey().equals("title") || at.getKey().equals("alt") || at.getKey().equals("role")) {
						b++;
						aTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//a[@" + at.getKey() + "='"
								+ at.getValue() + "']";
						log.info("Link Element with " + at.getKey() + " =  " + aTag);
						//log.info("Link Element with " + at.getKey() + " =  " + aTag);
						break;
					} else if (at.getKey().equals("aria-label")) {
						c++;
						aTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//a[@" + at.getKey() + "='"
								+ at.getValue() + "']";
						// log.info("Element with id/title/alt/role " + aTag);
					
						log.info("Link Element with " + at.getKey() + " =  " + aTag);

						break;
					}
					// get link with class attribute value
					else if (at.getKey().equals("class")) {
						int count = 0;
						if (at.getValue().contains("-")) {
							String[] splitbyIp = at.getValue().split("-");
							if (splitbyIp != null) {
								count = count + splitbyIp.length;
							}
						}
						if (at.getValue().contains("_")) {
							String[] splitbyUc = at.getValue().split("_");
							if (splitbyUc != null) {
								count = count + splitbyUc.length;
							}
						}

						if (at.getValue().length() < 10 || count < 3) {
							d++;
							aTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//a[@" + at.getKey() + "='"
									+ at.getValue() + "']";
							// log.info("Element with class" + aTag);
							break;

						}
						log.info("Link Element with " + at.getKey() + " =  " + aTag);

					}

					// get links that don't match above classifications
					else {
						aTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//a[@" + at.getKey() + "='"
								+ at.getValue() + "']";
						log.info("Link Element with " + at.getKey() + " =  " + aTag);

						break;
						// log.info("anchor tag wi: " + aTag);
						// log.info("Element with Text" + aTag);

					}
				}
			}
			log.info("A TAG ===>" + aTag);
			int lNameInc = 1;
			if (aTag != null && aTag.contains("'")) {
				if (aTag.length() > 1) {
					String[] locName = aTag.split("'");
					locName[locName.length - 2] = locName[locName.length - 2].replaceAll(" ", "").trim();
					for (Locators locator : locators) {
						// log.info("Changing value: "+locator.locatorName);
						// log.info(" Compare Value: "+locName[locName.length-2]);
						if (locator.locatorName.equalsIgnoreCase(locName[locName.length - 2])) {
							locName[locName.length - 2] = locName[locName.length - 2] + "_" + lNameInc++;
							// log.info("after Compare: "+locName[locName.length-2]);
						}
					}
					loc = new Locators(locName[locName.length - 2] + "_Link_" + section + "_" + page, "xpath", aTag);
					locators.add(loc);
					log.info("Locator value " + aTag);
				}
			} else {
				// log.info(aTag);
			}
		}
		log.info("\t\t#Total Links with linkText: " + a);
		log.info("\t\t#Total Links with id/title/alt/role: " + b);
		log.info("\t\t#Total Links with id: " + id);

		log.info("\t\t#Total Links with href: " + c);
		log.info("\t\t#Total Links with class: " + d);
		log.info("\t\t#Total Links with childTags with text: " + e);
		// log.info(locators.toString());
		int totalLocators = a + b + c + d + e + id;
		if (totalLocators > 0) {
			String sheetName = "AllLinks";
			writeToExcel(locatorExcelPath, sheetName, locators);
			log.info(
					"\n\t\t" + totalLocators + " Link Locators Written to Excel file Sheet: " + sheetName + ". \n");
		} else {
			log.info("\n\t\t" + totalLocators + " Link Locators Written to Excel file.\n");
		}
	}

	public static void getAllInputElementsInSection(Elements inputElements) throws IOException {
		List<Element> inputTags = inputElements;
		// All a tags with linkTexts
		int a = 0, b = 0, c = 0, d = 0, e = 0, f = 0;
		List<Locators> locators = new ArrayList<Locators>();
		// int inputElementsSize = inputTags.size();
		// log.info("\t\t\tTotal Input: " + inputElementsSize);

		// All a tags with linkText
		for (int i = 0; i < inputTags.size(); i++) {
			Element inputTag = inputTags.get(i);
			Attributes att = inputTag.attributes();
			List<Attribute> attributeList = att.asList();
			// log.info(asList);
			String iTag = null;

			for (Attribute at : attributeList) {
				if (inputTag.attr("type").length() > 2 && !inputTag.attr("type").equals("hidden")) {

					if (inputTag.attr("id").length() > 1) {
						a++;
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//input[@" + "id" + "='"
								+ inputTag.attr("id") + "' and @type='" + inputTag.attr("type") + "']";
						// log.info(iTag);
						break;
					} else if (inputTag.attr("name").length() > 1) {
						b++;
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//input[@" + "name" + "='"
								+ inputTag.attr("name") + "' and @type='" + inputTag.attr("type") + "']";
						// log.info(inputTag);
						break;
					} else if (at.getKey().equals("value") || at.getKey().equals("placeholder")
							|| at.getKey().equals("aria-label")) {
						c++;
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//input[@" + at.getKey() + "='"
								+ at.getValue() + "']";
						// log.info(iTag);
						break;
					} else if (inputTag.hasText()) {
						if (inputTag.ownText().length() > 1) {
							d++;
							iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//input[contains(text(),'"
									+ inputTag.ownText() + "')]";
							// log.info(iTag);
							break;
						} else {
							e++;
							iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//a/"
									+ inputTag.child(0).tagName() + "[contains(text(),'" + inputTag.text() + "')]";
							// log.info(e+" Element with Child Tag with Text: " +aTag);
							break;
						}
					} else if (at.getKey().equals("class")) {
						int count = 0;
						if (at.getValue().contains("-")) {
							String[] splitbyIp = at.getValue().split("-");
							if (splitbyIp != null) {
								count = count + splitbyIp.length;
							}
						}
						if (at.getValue().contains("_")) {
							String[] splitbyUc = at.getValue().split("_");
							if (splitbyUc != null) {
								count = count + splitbyUc.length;
							}
						}
						if (at.getValue().length() < 10 || count < 3) {

							f++;
							iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//input[@" + at.getKey()
									+ "='" + at.getValue() + "' and @type='" + inputTag.attr("type") + "']";
							// log.info(iTag);
							break;
						}
					} else {
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//input[@" + at.getKey() + "='"
								+ at.getValue() + "']";
						// log.info(iTag);
						break;
					}
				}

			}
			int lNameInc = 1;
			if (iTag != null && iTag.contains("'")) {
				if (iTag.length() > 1) {
					String[] locName = iTag.split("'");
					locName[locName.length - 4] = locName[locName.length - 4].replaceAll(" ", "").trim();
					for (Locators locator : locators) {
						// log.info("Changing value: "+locator.locatorName);
						// log.info(" Compare Value: "+locName[locName.length-2]);
						if (locator.locatorName.equalsIgnoreCase(locName[locName.length - 4])) {

							locName[locName.length - 4] = locName[locName.length - 4] + "_" + lNameInc++;
							/// log.info("after Compare: "+locName[locName.length-2]);
						}

					}
					loc = new Locators(
							locName[locName.length - 4].substring(0, 1).toUpperCase()
									+ locName[locName.length - 4].substring(1) + "_Input_" + section + "_" + page,
							"xpath", iTag);
					locators.add(loc);
				}
			} else {
				// log.info(aTag);
			}
		}
		log.info("\t\t#Total inputTags with id: " + a);
		log.info("\t\t#Total inputTags with name: " + b);
		log.info("\t\t#Total inputTags with value/placeholder/area-label: " + c);
		log.info("\t\t#Total inputTags with hasText - OwnText: " + d);
		log.info("\t\t#Total inputTags with childTags with text: " + e);
		log.info("\t\t#Total inputTags with class: " + f);
		// log.info(locators.toString());
		int totalLocators = a + b + c + d + e;
		if (totalLocators > 0) {
			String sheetName = "AllInputs";
			writeToExcel(locatorExcelPath, sheetName, locators);
			log.info(
					"\n\t\t" + totalLocators + " Link Locators Written to Excel file Sheet: " + sheetName + ". \n");
		} else {
			log.info("\n\t\t" + totalLocators + " Link Locators Written to Excel file.\n");
		}
	}

	public static void getAllHeaderTagElement(Elements headerElements, String selectionHeader) throws IOException {

		List<Element> inputTags = headerElements;
		// All a tags with linkText
		int a = 0, b = 0, c = 0, d = 0;

		List<Locators> locators = new ArrayList<Locators>();
		// int inputElementsSize = inputTags.size();
		// log.info("Total Header: " + inputElementsSize);

		// All a tags with linkText
		for (int i = 0; i < inputTags.size(); i++) {
			Element inputTag = inputTags.get(i);
			Attributes att = inputTag.attributes();
			List<Attribute> attributeList = att.asList();
			// log.info(asList);
			String iTag = null;

			for (Attribute at : attributeList) {

				if (inputTag.attr("id").length() > 1) {
					a++;
					iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//" + selectionHeader + "[@" + "id"
							+ "='" + inputTag.attr("id") + "']";
					// log.info(iTag);
					break;
				} else if (inputTag.attr("name").length() > 1) {
					b++;
					iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//" + selectionHeader + "[@" + "name"
							+ "='" + inputTag.attr("name") + "']";
					// log.info(inputTag);
					break;
				} else if (inputTag.hasText()) {
					c++;
					iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//" + selectionHeader
							+ "[contains(text()," + "'" + inputTag.text() + "')]";

					break;

				} else if (at.getKey().equals("class")) {
					int count = 0;
					if (at.getValue().contains("-")) {
						String[] splitbyIp = at.getValue().split("-");
						if (splitbyIp != null) {
							count = count + splitbyIp.length;
						}
					}
					if (at.getValue().contains("_")) {
						String[] splitbyUc = at.getValue().split("_");
						if (splitbyUc != null) {
							count = count + splitbyUc.length;
						}
					}
					if (at.getValue().length() < 15 || count < 6) {

						d++;
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//" + selectionHeader + "[@"
								+ at.getKey() + "='" + at.getValue() + "']";
						// log.info(iTag);
						break;
					}
				} else {
					iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//" + selectionHeader + "[@"
							+ at.getKey() + "='" + at.getValue() + "']";
					// log.info(iTag);
					break;
				}
			}
			int lNameInc = 1;
			if (iTag != null && iTag.contains("'")) {
				if (iTag.length() > 1) {
					String[] locName = iTag.split("'");
					locName[locName.length - 2] = locName[locName.length - 2].replaceAll(" ", "").trim();
					for (Locators locator : locators) {
						// log.info("Changing value: "+locator.locatorName);
						// log.info(" Compare Value: "+locName[locName.length-2]);
						if (locator.locatorName.equalsIgnoreCase(locName[locName.length - 2])) {

							locName[locName.length - 2] = locName[locName.length - 2] + "_" + lNameInc++;
							/// log.info("after Compare: "+locName[locName.length-2]);
						}
					}
					loc = new Locators(
							locName[locName.length - 2].substring(0, 1).toUpperCase()
									+ locName[locName.length - 2].substring(1) + "_Header_" + section + "_" + page,
							"xpath", iTag);
					locators.add(loc);
				}
			} else {
				// log.info(aTag);
			}
		}
		log.info("\t\t#Total headerTags with id: " + a);
		log.info("\t\t#Total headerTags with name: " + b);
		log.info("\t\t#Total headerTags with hasText - OwnText: " + c);
		log.info("\t\t#Total header with class: " + d);
		// log.info(locators.toString());
		int totalLocators = a + b + c + d;
		if (totalLocators > 0) {
			String sheetName = "AllHeaders";
			writeToExcel(locatorExcelPath, sheetName, locators);
			log.info(
					"\n\t\t" + totalLocators + " Header Locators Written to Excel file Sheet: " + sheetName + ". \n");
		} else {
			log.info("\n\t\t" + totalLocators + " Header Locators Written to Excel file.\n");
		}
	}

	public static void getAllButtonElementsInSection(Elements inputElements) throws IOException {

		List<Element> inputTags = inputElements;
		// All a tags with linkText
		int a = 0, b = 0, c = 0, d = 0, e = 0, f = 0;

		List<Locators> locators = new ArrayList<Locators>();
		// int inputElementsSize = inputTags.size();
		// log.info("Total Button: " + inputElementsSize);

		// All a tags with linkText
		for (int i = 0; i < inputTags.size(); i++) {
			Element inputTag = inputTags.get(i);
			Attributes att = inputTag.attributes();
			List<Attribute> attributeList = att.asList();
			// log.info(asList);
			String iTag = null;

			for (Attribute at : attributeList) {
				if (inputTag.attr("type").equals("submit")) {

					if (inputTag.attr("id").length() > 1) {
						a++;
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//button[@" + "id" + "='"
								+ inputTag.attr("id") + "' and @type='" + inputTag.attr("type") + "']";
						// log.info(iTag);
						break;
					} else if (inputTag.attr("name").length() > 1) {
						b++;
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//button[@" + "name" + "='"
								+ inputTag.attr("name") + "' and @type='" + inputTag.attr("type") + "']";
						// log.info(inputTag);
						break;
					} else if (at.getKey().equals("value") || at.getKey().equals("aria-label")) {
						c++;
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//button[@" + at.getKey() + "='"
								+ at.getValue() + "']";
						// log.info(iTag);
						break;
					} else if (inputTag.hasText()) {
						if (inputTag.ownText().length() > 1) {
							d++;
							iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//button[contains(text(),'"
									+ inputTag.ownText() + "')]";
							// log.info(iTag);
							break;
						} else {
							e++;
							iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//button/"
									+ inputTag.child(0).tagName() + "[contains(text(),'" + inputTag.text() + "')]";
							// log.info(e+" Element with Child Tag with Text: " +aTag);
							break;
						}
					} else if (at.getKey().equals("class")) {
						int count = 0;
						if (at.getValue().contains("-")) {
							String[] splitbyIp = at.getValue().split("-");
							if (splitbyIp != null) {
								count = count + splitbyIp.length;
							}
						}
						if (at.getValue().contains("_")) {
							String[] splitbyUc = at.getValue().split("_");
							if (splitbyUc != null) {
								count = count + splitbyUc.length;
							}
						}
						if (at.getValue().length() < 10 || count < 3) {

							f++;
							iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//button[@" + at.getKey()
									+ "='" + at.getValue() + "' and @type='" + inputTag.attr("type") + "']";
							// log.info(iTag);
							break;
						}
					} else {
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//button[@" + at.getKey() + "='"
								+ at.getValue() + "']";
						// log.info(iTag);
						break;
					}
				}
			}
			int lNameInc = 1;
			if (iTag != null && iTag.contains("'")) {
				if (iTag.length() > 1) {
					String[] locName = iTag.split("'");
					locName[locName.length - 2] = locName[locName.length - 2].replaceAll(" ", "").trim();
					for (Locators locator : locators) {
						// log.info("Changing value: "+locator.locatorName);
						// log.info(" Compare Value: "+locName[locName.length-2]);
						if (locator.locatorName.equalsIgnoreCase(locName[locName.length - 2])) {

							locName[locName.length - 2] = locName[locName.length - 2] + "_" + lNameInc++;
							/// log.info("after Compare: "+locName[locName.length-2]);
						}
					}
					loc = new Locators(
							locName[locName.length - 2].substring(0, 1).toUpperCase()
									+ locName[locName.length - 2].substring(1) + "_Button_" + section + "_" + page,
							"xpath", iTag);
					locators.add(loc);
				}
			} else {
				// log.info(aTag);
			}
		}
		log.info("\t\t#Total buttonTags with id: " + a);
		log.info("\t\t#Total buttonTags with name: " + b);
		log.info("\t\t#Total buttonTags with value/area-label: " + c);
		log.info("\t\t#Total buttonTags with hasText - OwnText: " + d);
		log.info("\t\t#Total buttonTags with childTags with text: " + e);
		log.info("\t\t#Total buttonTags with class: " + f);
		// log.info(locators.toString());
		int totalLocators = a + b + c + d + e + f;
		if (totalLocators > 0) {
			String sheetName = "AllButtons";
			writeToExcel(locatorExcelPath, sheetName, locators);
			log.info("\n\t\t" + totalLocators + " ButtonTag Locators Written to Excel file Sheet: "
					+ sheetName + ". \n");
		} else {
			log.info("\n\t\t" + totalLocators + " ButtonTag Locators Written to Excel file.\n");
		}
	}

	public static void getAllImageElementsInSection(Elements inputElements) throws IOException {

		List<Element> inputTags = inputElements;
		// All a tags with linkText
		int a = 0, b = 0, c = 0, d = 0, e = 0, f = 0;

		List<Locators> locators = new ArrayList<Locators>();
		// int inputElementsSize = inputTags.size();
		// log.info("Total Image: " + inputElementsSize);

		// All a tags with linkText
		for (int i = 0; i < inputTags.size(); i++) {
			Element inputTag = inputTags.get(i);
			Attributes att = inputTag.attributes();
			List<Attribute> attributeList = att.asList();
			// log.info(asList);
			String iTag = null;

			for (Attribute at : attributeList) {

				if (inputTag.attr("id").length() > 1) {
					b++;
					iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//img[@" + "id" + "='"
							+ inputTag.attr("id") + "']";
					// log.info(inputTag);
					break;
				} else if (at.getKey().equals("name") || at.getKey().equals("alt")) {
					c++;
					iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//img[@" + at.getKey() + "='"
							+ at.getValue() + "']";
					// log.info(iTag);
					break;
				}

				else if (inputTag.hasText()) {
					if (inputTag.ownText().length() > 1) {
						d++;
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//img[contains(text(),'"
								+ inputTag.ownText() + "')]";
						// log.info(iTag);
						break;

					} else if (at.getKey().equals("class")) {
						int count = 0;
						if (at.getValue().contains("-")) {
							String[] splitbyIp = at.getValue().split("-");
							if (splitbyIp != null) {
								count = count + splitbyIp.length;
							}
						}
						if (at.getValue().contains("_")) {
							String[] splitbyUc = at.getValue().split("_");
							if (splitbyUc != null) {
								count = count + splitbyUc.length;
							}
						}
						if (at.getValue().length() < 10 || count < 3) {

							f++;
							iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//img[@" + at.getKey() + "='"
									+ at.getValue() + "']";
							// log.info(iTag);
							break;
						}
					} else {
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//img[@" + at.getKey() + "='"
								+ at.getValue() + "']";
						// log.info(iTag);
						break;
					}
				} else if (inputTag.attr("src").length() > 1) {
					a++;
					String src = inputTag.attr("src");
					String[] split = src.split("/");

					iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//img[contains(@src,'"
							+ split[split.length - 1] + "')]";
					// log.info(iTag);
					break;
				}

				else {
					// log.info(aTag);
				}

			}

			int lNameInc = 0;
			if (iTag != null && iTag.contains("'")) {
				if (iTag.length() > 1) {
					String[] locName = iTag.split("'");
					locName[locName.length - 2] = locName[locName.length - 2].replaceAll(" ", "").trim();
					for (Locators locator : locators) {
						// log.info("Changing value: "+locator.locatorName);
						// log.info(" Compare Value: "+locName[locName.length-2]);
						if (locator.locatorName.equalsIgnoreCase(locName[locName.length - 2])) {

							locName[locName.length - 2] = locName[locName.length - 2] + "_" + lNameInc++;
							/// log.info("after Compare: "+locName[locName.length-2]);
						}
					}
					loc = new Locators(
							locName[locName.length - 2].substring(0, 1).toUpperCase()
									+ locName[locName.length - 2].substring(1) + "_Image_" + section + "_" + page,
							"xpath", iTag);
					locators.add(loc);
				}
			}
		}
		log.info("\t\t#Total imageTags with id: " + a);
		log.info("\t\t#Total imageTags  with name: " + b);
		log.info("\t\t#Total imageTags  with value/area-label: " + c);
		log.info("\t\t#Total imageTags  with hasText - OwnText: " + d);
		log.info("\t\t#Total imageTags  with childTags with text: " + e);
		log.info("\t\t#Total imageTags  with class: " + f);
		// log.info(locators.toString());
		int totalLocators = a + b + c + d + e + f;
		if (totalLocators > 0) {
			String sheetName = "AllImages";
			writeToExcel(locatorExcelPath, sheetName, locators);
			log.info(
					"\n\t\t" + totalLocators + " ImageTag Locators Written to Excel file Sheet: " + sheetName + ". \n");
		} else {
			log.info("\n\t\t" + totalLocators + " ImageTag Locators Written to Excel file.\n");
		}
	}

	public static void getAllSelectTagElement(Elements selectElements) throws Throwable {

		List<Element> inputTags = selectElements;
		// All a tags with linkText
		int a = 0, b = 0, c = 0, d = 0;

		List<Locators> locators = new ArrayList<Locators>();
		// int inputElementsSize = inputTags.size();
		// log.info("Total Select: " + inputElementsSize);

		// All a tags with linkText
		for (int i = 0; i < inputTags.size(); i++) {
			Element inputTag = inputTags.get(i);
			Attributes att = inputTag.attributes();
			List<Attribute> attributeList = att.asList();
			// log.info(asList);
			String iTag = null;

			for (Attribute at : attributeList) {

				if (inputTag.attr("id").length() > 1) {
					a++;
					iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//select[@" + "id" + "='"
							+ inputTag.attr("id") + "']";
					// log.info(iTag);
					break;
				} else if (inputTag.attr("name").length() > 1) {
					b++;
					iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//select[@" + "name" + "='"
							+ inputTag.attr("name") + "']";
					// log.info(inputTag);
					break;
				} else if (at.getKey().equals("title")) {
					c++;
					iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//select[@" + at.getKey() + "='"
							+ at.getValue() + "']";
					// log.info(iTag);
					break;
				} else if (at.getKey().equals("class")) {
					int count = 0;
					if (at.getValue().contains("-")) {
						String[] splitbyIp = at.getValue().split("-");
						if (splitbyIp != null) {
							count = count + splitbyIp.length;
						}
					}
					if (at.getValue().contains("_")) {
						String[] splitbyUc = at.getValue().split("_");
						if (splitbyUc != null) {
							count = count + splitbyUc.length;
						}
					}
					if (at.getValue().length() < 10 || count < 3) {

						d++;
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//select[@" + at.getKey() + "='"
								+ at.getValue() + "']";
						// log.info(iTag);
						break;
					}
				} else {
					iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//select[@" + at.getKey() + "='"
							+ at.getValue() + "']";
					// log.info(iTag);
					break;
				}
			}
			int lNameInc = 0;
			if (iTag != null && iTag.contains("'")) {
				if (iTag.length() > 1) {
					String[] locName = iTag.split("'");
					locName[locName.length - 2] = locName[locName.length - 2].replaceAll(" ", "").trim();
					for (Locators locator : locators) {
						// log.info("Changing value: "+locator.locatorName);
						// log.info(" Compare Value: "+locName[locName.length-2]);
						if (locator.locatorName.equalsIgnoreCase(locName[locName.length - 2])) {

							locName[locName.length - 2] = locName[locName.length - 2] + "_" + lNameInc++;
							/// log.info("after Compare: "+locName[locName.length-2]);
						}
					}
					loc = new Locators(
							locName[locName.length - 2].substring(0, 1).toUpperCase()
									+ locName[locName.length - 2].substring(1) + "_Select_" + section + "_" + page,
							"xpath", iTag);
					locators.add(loc);
				}
			} else {
				// log.info(aTag);
			}
		}
		log.info("\t\t#Total selectTags with id: " + a);
		log.info("\t\t#Total selectTags with name: " + b);
		log.info("\t\t#Total selectTags with title: " + c);
		log.info("\t\t#Total selectTags with class: " + d);
		// log.info(locators.toString());
		int totalLocators = a + b + c + d;
		if (totalLocators > 0) {
			String sheetName = "AllSelects";
			writeToExcel(locatorExcelPath, sheetName, locators);
			log.info("\n\t\t" + totalLocators + " SelectTag Locators Written to Excel file Sheet: "
					+ sheetName + ". \n");
		} else {
			log.info("\n\t\t" + totalLocators + " SelectTag Locators Written to Excel file.\n");
		}
	}

	public static void getAllSpanElements(Elements selectElements) throws IOException {

		List<Element> spanTags = selectElements;
		// All a tags with linkText
		int a = 0, b = 0, c = 0, d = 0, id = 0;

		List<Locators> locators = new ArrayList<Locators>();
		// int inputElementsSize = inputTags.size();
		// log.info("\t\tTotal span: " + inputElementsSize);

		// All a tags with linkText
		for (int i = 0; i < spanTags.size(); i++) {
			Element spanTag = spanTags.get(i);
			Attributes att = spanTag.attributes();
			List<Attribute> attributeList = att.asList();
			// log.info(asList);
			String iTag = null;

			for (Attribute at : attributeList) {
				if (spanTag.attr("id").length() > 2) {
					Pattern pattern = Pattern.compile("[0-9]+");
					Matcher matcher = pattern.matcher(spanTag.attr("id"));
					String match = null;
					// Find all matches
					while (matcher.find()) {
						// Get the matching string
						match = matcher.group();
					}
					if (match == null) {
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//span[@" + "id" + "='"
								+ spanTag.attr("id") + "']";
						log.info("Span Element with id  =  " + iTag);
						id++;
						break;
					}

				} else if (at.getValue().length() > 1 && spanTag.hasText()) {

					if (spanTag.ownText().length() > 1) {
						d++;
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//span[contains(text()," + "'"
								+ spanTag.ownText() + "')]";
						// log.info(iTag);

					} else if (spanTag.attr("name").length() > 1) {
						b++;
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//span[@" + "name" + "='"
								+ spanTag.attr("name") + "']";
						// log.info(inputTag);
						break;
					} else if (at.getKey().equals("class")) {
						int count = 0;
						if (at.getValue().contains("-")) {
							String[] splitbyIp = at.getValue().split("-");
							if (splitbyIp != null) {
								count = count + splitbyIp.length;
							}
						}
						if (at.getValue().contains("_")) {
							String[] splitbyUc = at.getValue().split("_");
							if (splitbyUc != null) {
								count = count + splitbyUc.length;
							}
						}

						if (at.getValue().length() < 10 || count < 3) {
							c++;
							iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//a[@" + at.getKey() + "='"
									+ at.getValue() + "']";
							// log.info("Element with class" + aTag);
							break;

						}

					} else {
						d++;
						iTag = "//*[@" + parentTagKey + "='" + parentTagAttr + "']" + "//span[@" + at.getKey() + "='"
								+ at.getValue() + "']";
						// log.info(iTag);
						break;
					}
				}
			}

			int lNameInc = 0;
			if (iTag != null && iTag.contains("'")) {
				if (iTag.length() > 1) {
					String[] locName = iTag.split("'");
					locName[locName.length - 2] = locName[locName.length - 2].replaceAll(" ", "").trim();
					for (Locators locator : locators) {
						// log.info("Changing value: "+locator.locatorName);
						// log.info(" Compare Value: "+locName[locName.length-2]);
						if (locator.locatorName.equalsIgnoreCase(locName[locName.length - 2])) {

							locName[locName.length - 2] = locName[locName.length - 2] + "_" + lNameInc++;
							/// log.info("after Compare: "+locName[locName.length-2]);
						}
					}
					loc = new Locators(
							locName[locName.length - 2].substring(0, 1).toUpperCase()
									+ locName[locName.length - 2].substring(1) + "_Span_" + section + "_" + page,
							"xpath", iTag);
					locators.add(loc);
				}
			} else {
				// log.info(aTag);
			}
		}
		log.info("\t\t#Total spanTags with id: " + a);
		log.info("\t\t#Total spanTags with name: " + b);
		log.info("\t\t#Total spanTags with class: " + c);
		log.info("\t\t#Total spanTags with text: " + d);
		// log.info(locators.toString());
		int totalLocators = a + b + c + d + id;
		if (totalLocators > 0) {
			String sheetName = "AllSpans";
			writeToExcel(locatorExcelPath, sheetName, locators);
			log.info(
					"\n\t\t" + totalLocators + " Span Locators Written to Excel file Sheet: " + sheetName + ". \n");
		} else {
			log.info("\n\t\t" + totalLocators + " Span Locators Written to Excel file.\n");
		}
	}

	public static void getTagChildElementsbyId(String PageHtml, String attributeValue, boolean getSibling)
			throws Throwable {
		parentTagAttr = attributeValue;
		// Document doc = Jsoup.connect(PageHtml).get();
		Document doc = Jsoup.parse(PageHtml, "UTF-8");
		log.info("Page Title: " + doc.title());

		Element tagContent = doc.getElementById(attributeValue);

		Elements elements = null;
		if (tagContent.children().size() > 0) {
			log.info("**************************************");
			log.info("Node has children");
			log.info("**************************************");
			elements = tagContent.children();
		} else {
			log.info("\nNo direct child elements/nodes in specified node!\n");

			if (getSibling == true && tagContent.siblingElements().size() > 0) {
				log.info("**************************************");
				log.info("Looking into Sibling Elements");
				log.info("**************************************");
				elements = tagContent.siblingElements();
			} else {
				log.info("\nSibling elements empty or disabled!");
			}
		}
		if (elements != null && elements.size() > 0) {
			int i = 1;
			for (Element element : elements) {
				log.info(i++ + " | ---------------------------------------------");
				// log.info("cssSelector: " + element.cssSelector());
				log.info("Tags: ");

				Elements anchorElements = element.getElementsByTag("a");
				log.info("\t >Total Anchor Elements on this section: " + anchorElements.size());
				getAllLinksInSection(anchorElements);

				Elements inputElements = element.getElementsByTag("input");
				log.info("\t >Total input Elements on this section: " + inputElements.size());
				getAllInputElementsInSection(inputElements);

				Elements selectElements = element.getElementsByTag("select");
				log.info("\t >Total select Elements on this section: " + selectElements.size());
				getAllSelectTagElement(selectElements);

				Elements buttonElements = element.getElementsByTag("button");
				log.info("\t >Total button Elements on this section: " + buttonElements.size());
				getAllButtonElementsInSection(buttonElements);

				Elements imgElements = element.getElementsByTag("img");
				log.info("\t >Total img Elements on this section: " + imgElements.size());
				getAllImageElementsInSection(imgElements);

				// log.info("=====Header Element ======");
				Elements headerH1Elements = element.getElementsByTag("h1");
				if (headerH1Elements.size() >= 1) {
					// log.info("=====H1 Header Element ======");
					log.info("\t >Total Header Elements on this section: " + headerH1Elements.size());
					getAllHeaderTagElement(headerH1Elements, "h1");
				}

				Elements headerH2Elements = element.getElementsByTag("h2");
				if (headerH2Elements.size() >= 1) {
					// log.info("=====H2 Header Element ======");
					log.info("\t >Total Header Elements on this section: " + headerH2Elements.size());
					getAllHeaderTagElement(headerH2Elements, "h2");
				}

				Elements headerH3Elements = element.getElementsByTag("h3");
				if (headerH3Elements.size() >= 1) {
					// log.info("=====H3 Header Element ======");
					log.info("\t >Total Header Elements on this section: " + headerH3Elements.size());
					getAllHeaderTagElement(headerH3Elements, "h3");
				}

				Elements headerH4Elements = element.getElementsByTag("h4");
				if (headerH4Elements.size() >= 1) {
					// log.info("=====H4 Header Element ======");
					log.info("\t >Total Header Elements on this section: " + headerH4Elements.size());
					getAllHeaderTagElement(headerH4Elements, "h4");
				}

				Elements headerH5Elements = element.getElementsByTag("h5");
				if (headerH5Elements.size() >= 1) {
					// log.info("=====H5 Header Element ======");
					log.info("\t >Total Header Elements on this section: " + headerH5Elements.size());
					getAllHeaderTagElement(headerH5Elements, "h5");
				}

				Elements headerH6Elements = element.getElementsByTag("h6");
				if (headerH6Elements.size() >= 1) {
					// log.info("=====H6 Header Element ======");
					log.info("\t >Total Header Elements on this section: " + headerH6Elements.size());
					getAllHeaderTagElement(headerH6Elements, "h6");
				}

				Elements iFrameElements = element.getElementsByTag("iframe");
				log.info("\t >Total iFrame Elements on this section: " + iFrameElements.size());
				// TODO

				Elements formElements = element.getElementsByTag("form");
				log.info("\t >Total Form Elements on this section: " + formElements.size());
				// TODO

				log.info("\t--------------------------------------");
				Elements divElements = element.getElementsByTag("div");
				log.info("\t >Total div Elements on this section: " + divElements.size());
				Elements spanElements = element.getElementsByTag("span");
				log.info("\t >Total span Elements on this section: " + spanElements.size());
				getAllSpanElements(spanElements);
				log.info("\n\n");
			}
		}
	}

	public static void getTagChildElementsbyAttributeKeyValue(String PageHtml, String attributeKey,
			String attributeValue, boolean getSibling) throws Throwable {
		parentTagKey = attributeKey;
		parentTagAttr = attributeValue;
		// Document doc = Jsoup.connect(PageHtml).get();
		Document doc = Jsoup.parse(PageHtml, "UTF-8");
		log.info("Page Title: " + doc.title());

		Elements rootTagContent = doc.getElementsByAttributeValue(attributeKey, attributeValue);
		log.info("Root tag content : " + rootTagContent);
		Element tagContent = rootTagContent.first();
		Elements elements = null;
		if (tagContent.children().size() > 0) {
			log.info("**************************************");
			log.info("Node has children");
			log.info("**************************************");
			elements = tagContent.children();
		} else {
			log.info("\nNo direct child elements/nodes in specified node!\n");

			if (getSibling == true && tagContent.siblingElements().size() > 0) {
				log.info("**************************************");
				log.info("Looking into Sibling Elements");
				log.info("**************************************");
				elements = tagContent.siblingElements();
			} else {
				log.info("\nSibling elements empty or disabled!");
			}
		}
		if (elements != null && elements.size() > 0) {
			int i = 1;
			for (Element element : elements) {
				log.info(i++ + " | ---------------------------------------------");
				// log.info("cssSelector: " + element.cssSelector());
				log.info("Tags: ");

				Elements anchorElements = element.getElementsByTag("a");
				log.info("\t >Total Anchor Elements on this section: " + anchorElements.size());
				getAllLinksInSection(anchorElements);

				Elements inputElements = element.getElementsByTag("input");
				log.info("\t >Total input Elements on this section: " + inputElements.size());
				getAllInputElementsInSection(inputElements);

				Elements selectElements = element.getElementsByTag("select");
				log.info("\t >Total select Elements on this section: " + selectElements.size());
				getAllSelectTagElement(selectElements);

				Elements buttonElements = element.getElementsByTag("button");
				log.info("\t >Total button Elements on this section: " + buttonElements.size());
				getAllButtonElementsInSection(buttonElements);

				Elements imgElements = element.getElementsByTag("img");
				log.info("\t >Total img Elements on this section: " + imgElements.size());
				getAllImageElementsInSection(imgElements);

				// log.info("=====Header Element ======");
				Elements headerH1Elements = element.getElementsByTag("h1");
				if (headerH1Elements.size() >= 1) {
					// log.info("=====H1 Header Element ======");
					log.info("\t >Total Header Elements on this section: " + headerH1Elements.size());
					getAllHeaderTagElement(headerH1Elements, "h1");
				}

				Elements headerH2Elements = element.getElementsByTag("h2");
				if (headerH2Elements.size() >= 1) {
					// log.info("=====H2 Header Element ======");
					log.info("\t >Total Header Elements on this section: " + headerH2Elements.size());
					getAllHeaderTagElement(headerH2Elements, "h2");
				}

				Elements headerH3Elements = element.getElementsByTag("h3");
				if (headerH3Elements.size() >= 1) {
					// log.info("=====H3 Header Element ======");
					log.info("\t >Total Header Elements on this section: " + headerH3Elements.size());
					getAllHeaderTagElement(headerH3Elements, "h3");
				}

				Elements headerH4Elements = element.getElementsByTag("h4");
				if (headerH4Elements.size() >= 1) {
					// log.info("=====H4 Header Element ======");
					log.info("\t >Total Header Elements on this section: " + headerH4Elements.size());
					getAllHeaderTagElement(headerH4Elements, "h4");
				}

				Elements headerH5Elements = element.getElementsByTag("h5");
				if (headerH5Elements.size() >= 1) {
					// log.info("=====H5 Header Element ======");
					log.info("\t >Total Header Elements on this section: " + headerH5Elements.size());
					getAllHeaderTagElement(headerH5Elements, "h5");
				}

				Elements headerH6Elements = element.getElementsByTag("h6");
				if (headerH6Elements.size() >= 1) {
					// log.info("=====H6 Header Element ======");
					log.info("\t >Total Header Elements on this section: " + headerH6Elements.size());
					getAllHeaderTagElement(headerH6Elements, "h6");
				}

				Elements iFrameElements = element.getElementsByTag("iframe");
				log.info("\t >Total iFrame Elements on this section: " + iFrameElements.size());
				// TODO

				Elements formElements = element.getElementsByTag("form");
				log.info("\t >Total Form Elements on this section: " + formElements.size());
				// TODO

				log.info("\t--------------------------------------");
				Elements divElements = element.getElementsByTag("div");
				log.info("\t >Total div Elements on this section: " + divElements.size());
				Elements spanElements = element.getElementsByTag("span");
				log.info("\t >Total span Elements on this section: " + spanElements.size());
				getAllSpanElements(spanElements);
				log.info("\n\n");
			}
		}
	}

	public static void writeToExcel(String filePath, String sheetName, List<Locators> locators) throws IOException {
		try {
			XSSFWorkbook workbook = new XSSFWorkbook();
			if (new File(locatorExcelPath).exists()) {
				FileInputStream fin = new FileInputStream(new File(locatorExcelPath));
				workbook = new XSSFWorkbook(fin);

			}

			XSSFSheet sheet = null;
			try {
				log.info("============================================================================");
				sheet = workbook.getSheet(sheetName);
				log.info("\t\t\tSheet:" + sheetName + " Last Row Number: " + sheet.getLastRowNum());

			} catch (NullPointerException e) {
				// sheet = workbook.createSheet(sheetName+WriteCount++);
				sheet = workbook.createSheet(sheetName);
				log.info("\t\t\tCreating new sheet! " + sheetName);
				XSSFRow headerRow = sheet.createRow(0);
				Cell cell0 = headerRow.createCell(0);
				cell0.setCellValue("LocatorName");
				Cell cell1 = headerRow.createCell(1);
				cell1.setCellValue("LocatorType");
				Cell cell2 = headerRow.createCell(2);
				cell2.setCellValue("LocatorValue");
				XSSFRow urlRow = sheet.createRow(1);
				Cell cell3 = urlRow.createCell(0);
				cell3.setCellValue(appName);
				Cell cell4 = urlRow.createCell(1);
				cell4.setCellValue("url");
				Cell cell5 = urlRow.createCell(2);
				cell5.setCellValue(currentUrl);
			}

			int rowIndex = sheet.getLastRowNum() + 1;
			// List<String> fv = fetchExcelValues(filePath, sheetName);

			for (Locators locator : locators) {
				// log.info("====================Fetch
				// Values===========================");
				// boolean contains = fv.contains(locator.getLocatorName());

				String locatorName = locator.getLocatorName();
				String locatorType = locator.getLocatorType();
				String locatorValue = locator.getLocatorValue();
				// int i = 1;
				// log.info("==============="+ contains);

//				if (contains) {
//					locatorName = locator.getLocatorName()+"_"+i;
//					locatorValue = "("+locator.getLocatorValue()+")["+i+"]";
//					log.info("====================Update Values===========================");
//					updateDuplicateValueExcel(filePath, sheetName, locator.getLocatorName(), locatorName, locatorValue);
//					locatorName = locator.getLocatorName()+"_"+(i+1);
//					locatorValue = "("+locator.getLocatorValue()+")["+(i+1)+"]";
//				}
//				

				XSSFRow row = sheet.createRow(rowIndex++);
				// log.info("LocatorName: "+locator.getLocatorName()+"\t LocatorType:
				// "+locator.getLocatorType()+"\t LocatorValue: "+locator.getLocatorValue());
				row.createCell(0).setCellValue(locatorName);
				row.createCell(1).setCellValue(locatorType);
				row.createCell(2).setCellValue(locatorValue);
			}
			FileOutputStream outputStream = new FileOutputStream(filePath);
			workbook.write(outputStream);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static List<String> fetchExcelValues(String filepath, String sheetName)
			throws FileNotFoundException, IOException {
		List<String> li = null;
		try {
			li = new ArrayList<String>();
			Workbook wb = new XSSFWorkbook(new FileInputStream(new File(filepath)));
			Sheet sheet = wb.getSheet(sheetName);
			if (sheet != null) {
				for (int i = 0; i < sheet.getPhysicalNumberOfRows(); i++) {
					String stringCellValue = sheet.getRow(i).getCell(0).getStringCellValue();
					li.add(stringCellValue);
				}
			}

			Set<String> st = new HashSet<String>();
			st.addAll(li);

			li.removeAll(st);

		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			log.info("Excel file is not available to fetch");
		} catch (Exception e) {
			e.printStackTrace();
		}
		return li;

	}

	public static void updateDuplicateValueExcel(String filepath, String sheetName, String value, String updatedValue,
			String locator) throws FileNotFoundException, IOException {
		try {
			Workbook wb = new XSSFWorkbook(new FileInputStream(new File(filepath)));
			Sheet sheet = wb.getSheet(sheetName);
			for (int i = 0; i < sheet.getPhysicalNumberOfRows(); i++) {
				String stringCellValue = sheet.getRow(i).getCell(0).getStringCellValue();
				if (stringCellValue.equals(value)) {
					sheet.getRow(i).getCell(0).setCellValue(updatedValue);
					sheet.getRow(i).getCell(2).setCellValue(locator);

				}
			}
			FileOutputStream fout = new FileOutputStream(new File(filepath));
			wb.write(fout);
			wb.close();
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			log.info("Excel file is not available to fetch");
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	public static void main(String[] args) throws Throwable {

		getTagChildElementsbyAttributeKeyValue("https://www.royalcyber.com/company/contact-us/", "id",
				"contact-us-page", true); // getTagChildElementsbyId(BaseUrl,"page",false);
		// getTagChildElementsbyId(BaseUrl,"eNews",false);
		// getTagChildElementsbyId("headerWrapper");
		// getTagChildElementsbyId("header");
		// getTagChildElementsbyId("headerRow1");
		// getTagChildElementsbyId(BaseUrl,"headerRow2",false);
		// String x = savePage();
		// log.info("html: "+x);
		// getTagChildElementsbyId(BaseUrl,"Products-Menu",true);
		// getTagChildElementsbyId("https://www.facebook.com", "globalContainer",
		// false);
		// getTagChildElementsbyId("https://www.omega.com/en-us/",
		// "component8796110980156", false);

		// getAllLinksInPage();l
		// getAllInputElement();

		// Testing Block below

//		List<Locators> locators = new ArrayList<Locators>();
//		locators.add(new Locators("test1", "test1", "test1"));
//		locators.add(new Locators("test2", "test2", "test2"));
//		locators.add(new Locators("test3", "test3", "test3"));
//		locators.add(new Locators("test4", "test4", "test4"));
//		locators.add(new Locators("test5", "test5", "test5"));
//		
//		String TestFilePath = "D:/Workspaces/Trials/CucumberFramework/src/test/java/com/Cucumber/TestFiles/PageObjects/OfficeBrands/HomePage.xlsx";
//		
//		createExcel("OfficeBrands", "HomePage", "https://test.com");
//		
//		writeToExcel(TestFilePath,"AllLinks", locators);
//		
//		writeToExcel(TestFilePath,"AllLinks", locators);
//		locators.add(new Locators("testInput1", "testinput1", "testinput1"));
//		locators.add(new Locators("testInput2", "testinput2", "testinput2"));
//		locators.add(new Locators("testInput3", "testInput3", "testInput3"));
//		locators.add(new Locators("testInput4", "testInput4", "testInput4"));
//		locators.add(new Locators("testInput5", "testInput5", "testInput5"));
//		writeToExcel(TestFilePath,"AllInputs", locators);
//		writeToExcel(TestFilePath,"AllSelect", locators);
//		writeToExcel(TestFilePath,"AllSelect", locators);
		// savePage();
	}

}
