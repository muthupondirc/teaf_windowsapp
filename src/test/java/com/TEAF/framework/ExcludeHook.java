package com.TEAF.framework;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.List;

import org.apache.log4j.Logger;

import com.jayway.jsonpath.DocumentContext;
import com.jayway.jsonpath.JsonPath;

public class ExcludeHook {

	static Logger log = Logger.getLogger(ExcludeHook.class.getName());

	public static void removeBeforeAfterHookForPassedStatus(String filename) throws IOException {

		File f = new File(System.getProperty("user.dir") + "/src/test/java/com/TestResults/cucumber-report/" + filename
				+ ".json");
		// Collection<String> values = Hooks.scenarioStatus.values();

		List<Object> totalHooks = JsonPath.read(f, "$..elements[*].steps[*].after");
		for (int j = 0; j < totalHooks.size(); j++) {
			List<Object> read = JsonPath.read(f, "$..elements[" + j + "].steps[*].after");
			for (int i = 0; i < read.size(); i++) {
				List<Object> embedding = JsonPath.read(f, "$..elements[" + j + "].steps[" + i + "].embeddings");
				List<Object> afterHook = JsonPath.read(f,
						"$..elements[" + j + "].steps[" + i + "].after[*].embeddings");

				if (embedding.size() == 0 && afterHook.size()==0) {
					DocumentContext parse = JsonPath.parse(f);
					DocumentContext set = parse.delete("$..elements[" + j + "].steps[" + i + "].after");
					String jsonString = set.jsonString();
					java.nio.file.Files.write(Paths.get(f.getAbsolutePath()), jsonString.getBytes());

				}

			}
		}

		DocumentContext parse = JsonPath.parse(f);
		DocumentContext beforesc = parse.delete("$..elements[*].steps[*].before");
		String beforescString = beforesc.jsonString();
		java.nio.file.Files.write(Paths.get(f.getAbsolutePath()), beforescString.getBytes());

		DocumentContext beforeset = parse.delete("$..elements[*].before");
		String beforeString = beforeset.jsonString();
		java.nio.file.Files.write(Paths.get(f.getAbsolutePath()), beforeString.getBytes());

		DocumentContext afterset = parse.delete("$..elements[*].after");
		String afterString = afterset.jsonString();
		java.nio.file.Files.write(Paths.get(f.getAbsolutePath()), afterString.getBytes());

		log.info("Removed Hooks from Cucumber Report");

	}
}
