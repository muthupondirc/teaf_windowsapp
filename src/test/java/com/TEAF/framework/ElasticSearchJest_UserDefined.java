package com.TEAF.framework;

import org.json.simple.JSONObject;

import com.google.gson.JsonObject;

import io.searchbox.client.JestClient;
import io.searchbox.client.JestResult;

public class ElasticSearchJest_UserDefined {
	
	private JestClient client;
	
	private JsonObject jestResultJson;
	private String index;

	public JestClient getClient() {
		return client;
	}

	public void setClient(JestClient client) {
		this.client = client;
	}

	public JsonObject getJestResultJson() {
		return jestResultJson;
	}

	public void setJestResultJson(JsonObject jestResultJson) {
		this.jestResultJson = jestResultJson;
	}

	public String getIndex() {
		return index;
	}

	public void setIndex(String index) {
		this.index = index;
	}
	
	private String jestResponse;

	public String getJestResponse() {
		return jestResponse;
	}

	public void setJestResponse(String jestResponse) {
		this.jestResponse = jestResponse;
	}
	
	

}
